"AddonInfo"
{
	"tatics"
	{
		"MaxPlayers"		"3"
	}
  "TeamCount" "1"
  "maps"      "tatics"
  "IsPlayable"  "1"
  "tatics"
  {
    "MaxPlayers"                    "3"
  }
  "Default_Keys"
  {
    "01"
    {
      "Key"       "V"
      "Command"   "+ToggleEquipments"
      "Name"      "Toggle Equipments HUD"
    }
    "02"
    {
      "Key"       "Esc"
      "Command"   "+HideEquipments"
      "Name"      "Hide Equipments HUD"
    }
  }
}
