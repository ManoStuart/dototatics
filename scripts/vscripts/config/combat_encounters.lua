if GameRules.encounters == nil then
  GameRules.encounters = {
    ---------------------------------------------------------------
    --                      DAZZLE FOREST
    ---------------------------------------------------------------
    daz = {
      rate = 0.15,
      vec = {
        {
          exp = 25, cd = 10, gold = 20, ap = 0,
          units = {
            [3] = { -- BADGUYS
              "npc_dota_creature_kobold_datadriven",
              "npc_dota_creature_kobold_datadriven",
              "npc_dota_creature_kobold_datadriven",
              "npc_dota_creature_kobold_datadriven",
              "npc_dota_creature_kobold_datadriven",
            }
          }
        },
        {
          exp = 45, cd = 10, gold = 40, ap = 2,
          units = {
            [3] = { -- BADGUYS
              "npc_dota_creature_kobold_boss_datadriven",
              "npc_dota_creature_kobold_datadriven",
              "npc_dota_creature_kobold_datadriven",
              "npc_dota_creature_kobold_datadriven",
            }
          }
        },
        {
          exp = 40, cd = 10, gold = 30, ap = 1,
          units = {
            [3] = { -- BADGUYS
              "npc_dota_creature_kobold_soldier_datadriven",
              "npc_dota_creature_kobold_soldier_datadriven",
              "npc_dota_creature_kobold_datadriven",
              "npc_dota_creature_kobold_datadriven",
            }
          }
        },
        {
          exp = 40, cd = 10, gold = 25, ap = 2,
          units = {
            [3] = { -- BADGUYS
              "npc_dota_creature_gnoll_datadriven",
              "npc_dota_creature_gnoll_datadriven",
              "npc_dota_creature_gnoll_frost_datadriven",
              "npc_dota_creature_gnoll_frost_datadriven",
            }
          }
        },
        {
          exp = 65, cd = 10, gold = 60, ap = 2,
          units = {
            [3] = { -- BADGUYS
              "npc_dota_creature_gnoll_frost_datadriven",
              "npc_dota_creature_kobold_soldier_datadriven",
              "npc_dota_creature_kobold_datadriven",
              "npc_dota_creature_kobold_boss_datadriven",
            }
          }
        },
      }
    },

    ---------------------------------------------------------------
    --                      GREEN DRAGON
    ---------------------------------------------------------------
    gd1 = {
      rate = 0.2,
      vec = {
        {
          exp = 35, cd = 10, gold = 20, ap = 2,
          units = {
            [3] = { -- BADGUYS
              "npc_dota_creature_silly_dragon_datadriven",
            }
          }
        },
        {
          exp = 60, cd = 10, gold = 40, ap = 2,
          units = {
            [3] = { -- BADGUYS
              "npc_dota_creature_troll_high_priest_datadriven",
              "npc_dota_creature_troll_high_priest_datadriven",
              "npc_dota_creature_dark_troll_datadriven",
            }
          }
        },
        {
          exp = 65, cd = 10, gold = 60, ap = 3,
          units = {
            [3] = { -- BADGUYS
              "npc_dota_creature_dark_troll_datadriven",
              "npc_dota_creature_dark_troll_datadriven",
            }
          }
        },
        {
          exp = 40, cd = 10, gold = 30, ap = 2,
          units = {
            [3] = { -- BADGUYS
              "npc_dota_creature_dark_troll_datadriven",
              "npc_dota_creature_gnoll_frost_datadriven",
              "npc_dota_creature_gnoll_frost_datadriven",
            }
          }
        },
      }
    },

    gd2 = {
      rate = 0.25,
      vec = {
        {
          exp = 60, cd = 10, gold = 40, ap = 4,
          units = {
            [3] = { -- BADGUYS
              "npc_dota_creature_silly_dragon_datadriven",
              "npc_dota_creature_silly_dragon_datadriven",
            }
          }
        },
        {
          exp = 80, cd = 10, gold = 70, ap = 5,
          units = {
            [3] = { -- BADGUYS
              "npc_dota_creature_troll_high_priest_datadriven",
              "npc_dota_creature_troll_high_priest_datadriven",
              "npc_dota_creature_dark_troll_datadriven",
              "npc_dota_creature_dark_troll_datadriven",
            }
          }
        },
        {
          exp = 100, cd = 10, gold = 80, ap = 5,
          units = {
            [3] = { -- BADGUYS
              "npc_dota_creature_silly_dragon_datadriven",
              "npc_dota_creature_dark_troll_datadriven",
              "npc_dota_creature_dark_troll_datadriven",
            }
          }
        },
      }
    },

    gd3 = {
      rate = 0.35,
      vec = {
        {
          exp = 90, cd = 10, gold = 60, ap = 6,
          units = {
            [3] = { -- BADGUYS
              "npc_dota_creature_silly_dragon_datadriven",
              "npc_dota_creature_silly_dragon_datadriven",
              "npc_dota_creature_silly_dragon_datadriven",
            }
          }
        },
        {
          exp = 100, cd = 10, gold = 90, ap = 5,
          units = {
            [3] = { -- BADGUYS
              "npc_dota_creature_silly_dragon_datadriven",
              "npc_dota_creature_silly_dragon_datadriven",
              "npc_dota_creature_dark_troll_datadriven",
              "npc_dota_creature_dark_troll_datadriven",
            }
          }
        },
      }
    },

    greenDragon = {
      vec = {
        {units = {[3] = {"npc_dota_boss_green_dragon"}}, exp = 200, cd = 20, gold = 300, ap = 15, sound = 1},
      }
    },

    ---------------------------------------------------------------
    --                            DESERT
    ---------------------------------------------------------------
    desert1 = {
      rate = 0.15,
      vec = {
        {
          exp = 60, cd = 10, gold = 0, ap = 5,
          units = {
            [3] = { -- BADGUYS
              "npc_dota_creature_spiderling_datadriven",
              "npc_dota_creature_spiderling_datadriven",
              "npc_dota_creature_spiderling_datadriven",
              "npc_dota_creature_spiderling_datadriven",
              "npc_dota_creature_spiderling_datadriven",
              "npc_dota_creature_spiderling_datadriven",
              "npc_dota_creature_spiderling_datadriven",
              "npc_dota_creature_spiderling_datadriven",
              "npc_dota_creature_spiderling_datadriven",
              "npc_dota_creature_spiderling_datadriven",
            }
          },
          ambient = {"npc_dota_environment_sandstorm"},
        },
        {
          exp = 50, cd = 10, gold = 90, ap = 5,
          units = {
            [3] = { -- BADGUYS
              "npc_dota_creature_golem_datadriven",
              "npc_dota_creature_golem_datadriven",
            }
          },
          ambient = {"npc_dota_environment_sandstorm"},
        },
        {
          exp = 40, cd = 10, gold = 30, ap = 4,
          units = {
            [3] = { -- BADGUYS
              "npc_dota_creature_thunder_lizard_datadriven",
            }
          },
          ambient = {"npc_dota_environment_sandstorm"},
        },
        {
          exp = 50, cd = 10, gold = 0, ap = 12,
          units = {
            [3] = { -- BADGUYS
              "npc_dota_creature_dokkaebi_datadriven",
              "npc_dota_creature_boooofus_datadriven",
            }
          },
        },
        {
          exp = 80, cd = 10, gold = 0, ap = 8,
          units = {
            [3] = { -- BADGUYS
              "npc_dota_creature_boooofus_datadriven",
              "npc_dota_creature_boooofus_datadriven",
            }
          },
        },
      }
    },

    desert3 = {
      rate = 0.25,
      vec = {
        {
          exp = 80, cd = 10, gold = 0, ap = 7,
          units = {
            [3] = { -- BADGUYS
              "npc_dota_creature_spiderling_datadriven",
              "npc_dota_creature_spiderling_datadriven",
              "npc_dota_creature_spiderling_datadriven",
              "npc_dota_creature_spiderling_datadriven",
              "npc_dota_creature_spiderling_datadriven",
              "npc_dota_creature_spiderling_datadriven",
              "npc_dota_creature_spiderling_datadriven",
              "npc_dota_creature_spiderling_datadriven",
              "npc_dota_creature_spiderling_datadriven",
              "npc_dota_creature_spiderling_datadriven",
              "npc_dota_creature_spiderling_datadriven",
              "npc_dota_creature_spiderling_datadriven",
            }
          },
          ambient = {"npc_dota_environment_sandstorm"},
        },
        {
          exp = 80, cd = 10, gold = 110, ap = 8,
          units = {
            [3] = { -- BADGUYS
              "npc_dota_creature_golem_datadriven",
              "npc_dota_creature_golem_datadriven",
              "npc_dota_creature_boooofus_datadriven",
            }
          },
          ambient = {"npc_dota_environment_sandstorm"},
        },
        {
          exp = 90, cd = 10, gold = 60, ap = 8,
          units = {
            [3] = { -- BADGUYS
              "npc_dota_creature_thunder_lizard_datadriven",
              "npc_dota_creature_thunder_lizard_datadriven",
            }
          },
          ambient = {"npc_dota_environment_sandstorm"},
        },
        {
          exp = 80, cd = 10, gold = 0, ap = 20,
          units = {
            [3] = { -- BADGUYS
              "npc_dota_creature_dokkaebi_datadriven",
              "npc_dota_creature_dokkaebi_datadriven",
              "npc_dota_creature_boooofus_datadriven",
            }
          },
        },
        {
          exp = 90, cd = 10, gold = 60, ap = 6,
          units = {
            [3] = { -- BADGUYS
              "npc_dota_creature_thunder_lizard_datadriven",
              "npc_dota_creature_thunder_lizard_datadriven",
            },
            [2] = { -- NEUTRALS
              "npc_dota_jumo",
            }
          },
          ambient = {"npc_dota_environment_sandstorm"},
        },
      }
    },

    desertChapel = {
      vec = {{
        exp = 400, cd = 17, gold = 100, ap = 20, sound = 1,
        units = {[3] = {"npc_dota_creature_ghost_boss_datadriven"}},
        ambient = {"npc_dota_environment_ghost"},
      }}
    },

    ---------------------------------------------------------------
    --                          RED DRAGON
    ---------------------------------------------------------------
    desert4 = {
      rate = 0.25,
      vec = {
        {
          exp = 80, cd = 10, gold = 0, ap = 7,
          units = {
            [3] = { -- BADGUYS
              "npc_dota_creature_skelleton_datadriven",
              "npc_dota_creature_skelleton_datadriven",
              "npc_dota_creature_skelleton_datadriven",
              "npc_dota_creature_skelleton_datadriven",
              "npc_dota_creature_skelleton_datadriven",
              "npc_dota_creature_skelleton_datadriven",
              "npc_dota_creature_skelleton_datadriven",
            }
          },
          ambient = {"npc_dota_environment_sandstorm"},
        },
        {
          exp = 80, cd = 10, gold = 110, ap = 8,
          units = {
            [3] = { -- BADGUYS
              "npc_dota_creature_golem_datadriven",
              "npc_dota_creature_golem_datadriven",
              "npc_dota_creature_ig_dragon_datadriven",
            }
          },
          ambient = {"npc_dota_environment_sandstorm"},
        },
        {
          exp = 100, cd = 10, gold = 60, ap = 12,
          units = {
            [3] = { -- BADGUYS
              "npc_dota_creature_thunder_lizard_datadriven",
              "npc_dota_creature_skelleton_datadriven",
              "npc_dota_creature_skelleton_datadriven",
              "npc_dota_creature_skelleton_datadriven",
            }
          },
          ambient = {"npc_dota_environment_sandstorm"},
        },
        {
          exp = 140, cd = 10, gold = 80, ap = 15,
          units = {
            [3] = { -- BADGUYS
              "npc_dota_creature_boooofus_datadriven",
              "npc_dota_creature_ig_dragon_datadriven",
              "npc_dota_creature_boooofus_datadriven",
            }
          },
        },
      }
    },

    redDragon = {
      vec = {{
        exp = 200, cd = 17, gold = 300, ap = 15, sound = 1,
        units = {[3] = {"npc_dota_boss_red_dragon"}},
        ambient = {"npc_dota_environment_sandstorm"},
      }}
    },
  }

  GameRules.encounters.desert2 = GameRules.encounters.desert1
end