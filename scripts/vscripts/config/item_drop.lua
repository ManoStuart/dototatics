if GameRules.drops == nil then
  GameRules.drops = {
    ---------------------------------------------------------------
    --                      DAZZLE FOREST
    ---------------------------------------------------------------
    npc_dota_creature_kobold_datadriven = {
      {chance = 15, item = "item_pumpkin"},
    },
    npc_dota_creature_kobold_soldier_datadriven = {
      {chance = 15, item = "item_blade_alacrity_datadriven"},
      {chance = 20, item = "item_pumpkin"},
    },
    npc_dota_creature_kobold_boss_datadriven = {
      {chance = 35, item = "item_sage_mask_datadriven"},
      {chance = 50, item = "item_pumpkin"},
    },
    npc_dota_creature_gnoll_datadriven = {
      {chance = 40, item = "item_poison_coat"},
      {chance = 25, item = "item_grapefruit"},
    },
    npc_dota_creature_gnoll_frost_datadriven = {
      {chance = 23, item = "item_pumpkin"},
    },

    ---------------------------------------------------------------
    --                      GREEN DRAGON
    ---------------------------------------------------------------
    npc_dota_creature_silly_dragon_datadriven = {
      {chance = 15, item = "item_wings_datadriven"},
    },
    npc_dota_creature_dark_troll_datadriven = {
      {chance = 20, item = "item_ogre_axe_datadriven"},
      {chance = 30, item = "item_grapefruit"},
    },
    npc_dota_creature_troll_high_priest_datadriven = {
      {chance = 20, item = "item_staff_of_wizardry_datadriven"},
    },
    npc_dota_boss_green_dragon = {
      {chance = 15, item = "item_blood_helm_datadriven"},
      {chance = 100, item = "item_helm_datadriven"},
    },

    ---------------------------------------------------------------
    --                          DESERT
    ---------------------------------------------------------------
    npc_dota_creature_dokkaebi_datadriven = {
      {chance = 15, item = "item_slippers_datadriven"},
      {chance = 45, item = "item_pumpkin"},
    },
    npc_dota_creature_thunder_lizard_datadriven = {
      {chance = 15, item = "item_pumpkin"},
    },
    npc_dota_creature_spiderling_datadriven = {
      {chance = 13, item = "item_poison_coat"},
    },
    npc_dota_creature_golem_datadriven = {},
    npc_dota_creature_mini_golem_datadriven = {
      {chance = 35, item = "item_enchanted_boulder"},
    },
    npc_dota_creature_boooofus_datadriven = {
      {chance = 15, item = "item_fire_ring_datadriven"},
      {chance = 35, item = "item_grapefruit"},
    },

    ---------------------------------------------------------------
    --                       RED DRAGON
    ---------------------------------------------------------------
    npc_dota_creature_skelleton_datadriven = {},
    npc_dota_creature_ig_dragon_datadriven = {
      {chance = 15, item = "item_fire_scale"},
      {chance = 30, item = "item_pumpkin"},
    },
    npc_dota_boss_red_dragon = {
      {chance = 100, item = "item_vanguard_datadriven"},
    },

    ---------------------------------------------------------------
    --                    DESERT OMNIKNIGHT
    ---------------------------------------------------------------
    npc_dota_creature_mini_ghost_datadriven = {},
    npc_dota_creature_ghost_boss_datadriven = {
      {chance = 100, item = "item_medalion_datadriven"},
    },
  }
end

function GameRules:checkDrop(entity)
  if not entity or entity:IsNull() then return end

  local obj = GameRules.drops[entity:GetUnitName()]
  local items = {}

  -- Drop new Item from creep
  if obj then
    for _,v in pairs(obj) do
      if RandomInt(1, 100) < v.chance then
        if not v.func or v.func(entity) then
          items[#items+1] = {item = CreateItem(v.item, nil, nil)}
          items[#items].phys = CreateItemOnPositionForLaunch(entity:GetOrigin(), items[#items].item)
          break
        end
      end
    end

    -- Drop all items in creep's inventory
    for i = 0, 5 do
      local item = entity:GetItemInSlot(i)

      if item then
        local newItem = CreateItem(item:GetName(), entity, entity)
        items[#items+1] = {item = newItem}

        -- Store development
        newItem.ap = item.ap
        newItem.rpg = item.rpg

        -- Set consumables charges
        newItem:SetCurrentCharges(item:GetCurrentCharges())

        -- Drop item
        items[#items].phys = CreateItemOnPositionForLaunch(entity:GetOrigin(), newItem)

        entity:RemoveItem(item)
      end
    end

  -- Drop item from player
  elseif entity:GetPlayerOwnerID() ~= -1 then
    -- Remove gold
    entity:SpendGold(entity:GetGold() * 0.5, DOTA_ModifyGold_Death)

    local chance = 25

    -- Drop equiped items
    for k,v in pairs(entity.equip) do
      if v.item ~= nil and RandomInt(1, 100) < chance then
        items[#items+1] = GameRules.UnequipItem(entity, k, true)
        GameRules.Equip(entity, nil, k, false)

        -- Two handed shit
        if k == 0 and entity.equip[1] and entity.equip[1].modifier == nil then
          GameRules.Equip(entity, nil, 1, false)
        end

        chance = 5
      end
    end

    -- Drop inventory items
    for i = 0, 5 do
      local item = entity:GetItemInSlot(i)

      if item and RandomInt(1, 100) < chance then
        local newItem = CreateItem(item:GetName(), entity, entity)
        items[#items+1] = {item = newItem}

        -- Store development
        newItem.ap = item.ap
        newItem.rpg = item.rpg

        -- Set consumables charges
        newItem:SetCurrentCharges(item:GetCurrentCharges())

        -- Drop item
        items[#items].phys = CreateItemOnPositionForLaunch(entity:GetOrigin(), newItem)

        entity:RemoveItem(item)

        chance = 5
      end
    end
  end

  -- Add drop animation
  for _,v in pairs(items) do
    v.item:LaunchLoot(false, 200, 0.75, v.phys:GetOrigin() + RandomVector(RandomFloat(20, 100)))
  end

  -- Delete still dropped items in 15 seconds
  Timers:CreateTimer(45, function()
    for _,v in pairs(items) do
      if not v.phys:IsNull() then
        v.item:Destroy()
        v.phys:Destroy()
      end
    end
  end)
end