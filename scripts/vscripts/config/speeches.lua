if GameRules.speeches == nil then
  GameRules.speeches = {
    ---------------------------------------------------------------
    --                      MAIN CITY
    ---------------------------------------------------------------
    -- In this healing glow, I feel a presence of the Omniscience
    mainpriest = {
      {
        msg = "Oh my! let me see these wounds!",
        cond = function (entity)
          return entity:GetHealthPercent() <= 30
        end
      },
      {
        msg = {"The All-knowing One senses your regret.", "The All-knowing One protects us all.", "Blessings!"},
        cond = function (entity)
          return RandomInt(1,10) < 8
        end
      },
    },

    towerguard = {
      {
        msg = "Careful! You should not leave town unarmed!",
        cond = function (entity)
          return not entity.equip[0]
        end
      },
      {msg = {"good luck!", "the gods be with you!", "ganbare!"}}
    },

    mainpeasant = {
      {
        msg = {"Interaction is key in this game.", "Our king sure looks laid back.", "I once found a delicious pumpkin under a rock!"},
        cond = function (entity)
          return RandomInt(1,10) < 6
        end
      },
    },

    mainworker = {
      {
        msg = {"Argh!", "So tired!", "Timber!"},
        cond = function (entity)
          return RandomInt(1,10) < 3
        end
      },
    },

    mainguard = {
      {
        msg = {"Do not disturb your king!", "Bow to your highness!.", "Be quick peasant!"},
        cond = function (entity)
          return RandomInt(1,10) < 8
        end
      },
    },

    mainclient = {
      {
        msg = {"Hmm.. Should I go two handed?", "This shield looks so cool!.", "I wish they had a bigger sword.."},
        cond = function (entity)
          return RandomInt(1,10) < 6
        end
      },
    },

    ---------------------------------------------------------------
    --                      DAZZLE FOREST
    ---------------------------------------------------------------
    tribe_elder = {
      {
        msg = {"Our young prodigy!", "Dazzle!", "More dazzling than ever."},
        cond = function (entity)
          return entity:GetUnitName() == "npc_dota_hero_dazzle"
        end
      },
      {
        msg = {"Heal or harm!", "In the Nothl realm, brightness is dark, and the touch that heals a friend will fell a foe."},
        cond = function (entity)
          return entity.rpg.dazzle and RandomInt(1,10) < 6
        end
      },
      {
        msg = {"I convey your spirit to the Nothl realm.", "I've been to the place where darkness is light.", "Power of a priest!"},
        cond = function (entity)
          return RandomInt(1,10) < 6
        end
      },
    },

    tribeman = {
      {
        msg = {"These kobolds are a constant hassle.", "The Dezun order guides our steps.", "State your business with the elder."},
        cond = function (entity)
          return RandomInt(1,10) < 4
        end
      },
    },

    ---------------------------------------------------------------
    --                      DESERT CITY
    ---------------------------------------------------------------
    npc_dota_jumo = {
      {
        msg = "Me SARVA FIO!",
        cond = function (entity)
          return true
        end
      },
    },
  }
end