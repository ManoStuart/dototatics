if GameRules.movements == nil then
  GameRules.movements = {
    ---------------------------------------------------------------
    --                      MAIN CITY
    ---------------------------------------------------------------
    mainpriest = {
      {duration = 15},
      {next = {3,4,5,6}},
      {wp = "city_point_6", next = 1},
      {wp = "city_point_7", next = 1},
      {wp = "city_point_8", next = 1},
      {wp = "city_point_9", next = 1},
    },

    mainpeasant = {
      {next = {2,3,4}},
      {wp = "city_point_10", next = 5},
      {wp = "city_point_11", next = 6},
      {wp = "city_point_12", next = 6},
      {duration = 8},
      {duration = 8},
    },

    mainworker = {
      {next = {2,3,4}},
      {wp = "city_point_3", next = 5},
      {wp = "city_point_4", next = 5},
      {wp = "city_point_5", next = 5},
      {animation = "attackAnimation", duration = 1},
      {animation = "attackAnimation", duration = 1, next = {6, 6, 6, 6, 6, 6, 7, 8}},
      {wp = "city_point_1", next = 9},
      {wp = "city_point_2", next = 9},
      {animation = "attackAnimation", duration = 0.7},
    },

    mainclient = {
      {animation = "attackAnimation", duration = 1.5},
      {duration = 6, next = {1,2,2}},
    },

    ---------------------------------------------------------------
    --                      DAZZLE FOREST
    ---------------------------------------------------------------
    tribe_elder = {
      {animation = "idleAnimation", duration = 15},
      {next = {3,4,5}},
      {wp = "tribe_point_1", next = 1},
      {wp = "tribe_point_2", next = 1},
      {wp = "tribe_point_3", next = 1},
    },

    tribeman = {
      {duration = 10},
      {next = {3,4,5,6,7,8,9}},
      {wp = "tribe_point_4", next = 1},
      {wp = "tribe_point_5", next = 1},
      {wp = "tribe_point_6", next = 1},
      {wp = "tribe_point_7", next = 1},
      {wp = "tribe_point_8", next = 1},
      {wp = "tribe_point_9", next = 1},
      {wp = "tribe_point_10", next = 1},
    },

    ---------------------------------------------------------------
    --                      DESERT CITY
    ---------------------------------------------------------------
    npc_dota_jumo = {
      {wp = "desert_point_1"},
      {func = QuestManager.EnchantressRescue},
    },

    desertpeasant = {
      {duration = 10},
      {next = {3,4,5,6,7,8}},
      {wp = "desert_point_1", next = 1},
      {wp = "desert_point_2", next = 1},
      {wp = "desert_point_3", next = 1},
      {wp = "desert_point_4", next = 1},
      {wp = "desert_point_5", next = 1},
      {wp = "desert_point_6", next = 1},
    },
  }

  GameRules.movements.desertinn = GameRules.movements.desertpeasant
end

if GameRules.curMovements == nil then GameRules.curMovements = {} end

function GameRules.StopMovement(entity)
  GameRules.curMovements[entity:entindex()] = nil
end

function GameRules.StartMovement(entity, delay)
  local name = entity:GetName()
  if name == "npc_dota_creature" then name = entity:GetUnitName() end

  local mov = GameRules.movements[name]
  if not mov then return end

  GameRules.curMovements[entity:entindex()] = {
    cur = entity:Attribute_GetIntValue("movementStart", 1),
    mov = mov
  }

  Timers:CreateTimer(delay, function()
    GameRules.NextMovement(entity)
  end)
end

function GameRules.NextMovement(entity)
  local movement = GameRules.curMovements[entity:entindex()]
  if not movement then return end

  local aux = movement.mov[movement.cur]

  -- Disable iterations
  if (not aux and movement.cur == -1) or not entity or entity:IsNull() then return end

  -- simple branch
  if aux and aux.next then movement.cur = aux.next

  -- conditional branch
  elseif aux and aux.func then print() movement.cur = aux.func(entity)

  -- make movement a permanent cycle
  else movement.cur = (movement.cur % #movement.mov) + 1 end

  -- Random next movement if necessary
  if type(movement.cur) == "table" then movement.cur = movement.cur[RandomInt(1, #movement.cur)] end


  -- Order unit to move
  if aux and aux.wp then
    local pos = Entities:FindByName(nil, aux.wp)

    if pos then
      local order = {
        OrderType = DOTA_UNIT_ORDER_MOVE_TO_POSITION,
        UnitIndex = entity:entindex(),
        Position = pos:GetOrigin(),
      }
      ExecuteOrderFromTable( order )

      -- Execute Next Movement when unit reaches waypoint
      Timers:CreateTimer(0.5, function()
        if entity:IsNull() then return end
        if (entity:GetOrigin() - pos:GetOrigin()):Length2D() <= 80 then
          return GameRules.NextMovement(entity)
        end

        return 0.5
      end)
    end

  -- Change unit animation
  elseif aux and aux.animation and aux.duration then
    GameRules.Overseer:CastTarget(entity, aux.animation)

    -- Execute Next Movement on end duration
    Timers:CreateTimer(aux.duration, function()
      GameRules.Overseer:CastTarget(entity, "purgeAnimation")
      GameRules.NextMovement(entity)
    end)

  -- wait
  elseif aux and aux.duration then
    Timers:CreateTimer(aux.duration, function()
      GameRules.NextMovement(entity)
    end)

  else
    GameRules.NextMovement(entity)
  end
end