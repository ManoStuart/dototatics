function Interact(keys)
  local obj

  if keys.target then
    if keys.target:entindex() == keys.unit:entindex() then return end

    obj = keys.target
  else 
    obj = Entities:FindByClassnameNearest("info_target_instructor_hint", keys.target_points[1], 80)
  end

  if obj and not keys.unit.curInteraction then
    local name = obj:GetName()
    if name == "npc_dota_creature" then name = obj:GetUnitName() end

    -- Make sure the interaction exists
    local status, interaction = pcall(function () return require("interactions/" .. name) end)
    if not status then return print(interaction) end

    CombatManager:DisableAll(false)

    keys.unit.curInteraction = interaction
    keys.unit.curInteraction = keys.unit.curInteraction:start(keys.unit, obj)
  end
end

function CancelInteraction(keys)
  if not keys.unit.curInteraction then
    return keys.unit:SwapAbilities("dota_ability_interact", "dota_ability_cancel_interaction", true, false)
  end

  keys.unit.curInteraction:cancel(keys.unit)
end

-- Overseer purge callback
function PurgeAnimation(keys)
  keys.target:RemoveModifierByName("modifier_attack_animation")
  keys.target:RemoveModifierByName("modifier_walk_animation")
  keys.target:RemoveModifierByName("modifier_idle_rare_animation")
end