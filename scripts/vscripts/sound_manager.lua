if SoundManager == nil then
  SoundManager = class({})
  GameRules.SoundManager = SoundManager()

  GameRules.soundLengths = LoadKeyValues("scripts/sound_lengths.txt")
end

function SoundManager:PreviousAmbientSound(entity)
  if not entity or not entity.curSound then return end

  SoundManager:StartSound(entity, entity.curSound.previous, true, true)
end

function SoundManager:StartSound(entity, sound, loop, environment)
  if not entity or not entity.curSound then return end

  -- Same sound
  if entity.curSound.name == sound then return end

  if environment then
    entity.curSound.previous = entity.curSound.environment
    entity.curSound.environment = sound

    -- Do not change to environment when in combat or when playing one time sounds
    if entity.inCombat or entity.curSound.simple then return end
  end

  -- Stop current sound
  if entity.curSound.name then
    StopSoundEvent(entity.curSound.name, entity)
  end

  entity.curSound.count = entity.curSound.count + 1
  entity.curSound.name = sound

  -- Player want no fag sound
  if entity.curSound.mute then return end

  entity.curSound.simple = not loop

  local count = entity.curSound.count
  Timers:CreateTimer(0.5, function()
    if count ~= entity.curSound.count then return end
    StartSoundEvent(sound, entity)
  end)

  local time = GameRules.soundLengths[sound]
  if not time then return end
  time = time + 0.4

  if loop then
    -- loop shit up
    Timers:CreateTimer(time, function()
      if count ~= entity.curSound.count then return end

      StartSoundEvent(entity.curSound.name, entity)
      return time
    end)
  else
    -- Return to environment sound after done playing current
    Timers:CreateTimer(time, function()
      if count ~= entity.curSound.count then return end

      entity.curSound.simple = false
      SoundManager:StartSound(entity, entity.curSound.environment, true, true)
    end)
  end
end

function SoundManager:StopSounds(entity)
  -- Stop current sound
  if entity.curSound.name then
    StopSoundEvent(entity.curSound.name, entity)
  end

  entity.curSound.name = nil
end