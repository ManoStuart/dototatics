require( "overseer" )
require( "events" )
require( "utils" )

require( "combat_manager" )
require ("sound_manager")
require ("quest_manager")

require( "config/speeches" )
require( "config/movements" )
require( "config/item_drop" )

if COverthrowGameMode == nil then
	_G.COverthrowGameMode = class({}) -- put COverthrowGameMode in the global scope
end

function Precache( context )
  PrecacheResource("particle", "particles/units/heroes/hero_disruptor/disruptor_kf_wall.vpcf", context)
  PrecacheResource("soundfile", "soundevents/encounter_sounds.vsndevts", context)

  -- Dazzle Forest
  PrecacheResource("particle", "particles/units/heroes/hero_dazzle/dazzle_base_attack.vpcf", context)
  PrecacheItemByNameSync("dazzle_poison_touch_datadriven", context)

  -- Desert
  PrecacheResource("particle", "particles/units/heroes/hero_dragon_knight/dragon_knight_elder_dragon_fire.vpcf", context)
  PrecacheItemByNameSync("phantom_assassin_stifling_dagger_datadriven", context)
end

function Activate()
	print( "Doto Tatics is loaded." )

  GameRules.duringEvent = false

  GameRules:SetSameHeroSelectionEnabled(true)
  GameRules:SetPreGameTime(0)
  GameRules:SetHeroSelectionTime(0)
  GameRules:SetHeroRespawnEnabled(false)
  GameRules:SetFirstBloodActive(false)
  GameRules:SetUseUniversalShopMode(true)
  GameRules:SetGoldTickTime( 60.0 )
  GameRules:SetGoldPerTick( 0 )
  GameRules:SetUseCustomHeroXPValues(true)
  GameRules:SetUseBaseGoldBountyOnHeroes(true)
  GameRules:SetCustomGameTeamMaxPlayers(DOTA_TEAM_GOODGUYS, 3)
  GameRules:SetCustomGameTeamMaxPlayers(DOTA_TEAM_BADGUYS, 0)
  GameRules:LockCustomGameSetupTeamAssignment(true)
  GameRules:SetCustomGameSetupAutoLaunchDelay(3)
  GameRules:SetStartingGold(999999)
  
  local gameMode = GameRules:GetGameModeEntity()

  gameMode:SetBuybackEnabled(false)
  gameMode:SetRecommendedItemsDisabled(true)
  gameMode:SetCustomGameForceHero("npc_dota_hero_jakiro")
  gameMode:SetUnseenFogOfWarEnabled(true)
  gameMode:SetStashPurchasingDisabled(true)
  gameMode:SetFixedRespawnTime(0.2)
  gameMode:SetLoseGoldOnDeath(false)

  CustomGameEventManager:RegisterListener("finish_turn", GameRules.CombatManager.OnFinishTurn)
  CustomGameEventManager:RegisterListener("finish_interaction", OnFinishInteraction)
  CustomGameEventManager:RegisterListener("unequip", OnUnequipItem)
  CustomGameEventManager:RegisterListener("speech_choice", OnSpeechChoice)
  CustomGameEventManager:RegisterListener("toggle_sound", OnToggleSound)
end