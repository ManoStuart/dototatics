if QuestManager == nil then
  QuestManager = class({})
  GameRules.QuestManager = QuestManager()

  GameRules.rpg = {}
end

-------------------------------------------------------------
--                      Omniknight
-------------------------------------------------------------

function QuestManager.Omniknight(entity)
  if not entity then return end

  if entity.rpg.omniknight >= 4 then
    GameRules.ChangeHero(entity, "npc_dota_hero_omniknight")
  end
end

-------------------------------------------------------------
--                      Enchantress
-------------------------------------------------------------

-- 
function QuestManager.EnchantressRescue(entity)
  if not entity or entity.enchantress then return end

  entity.enchantress = true

  -- Change rescued movement pattern
  GameRules.movements.npc_dota_jumo = GameRules.movements.desertpeasant
  GameRules.StopMovement(entity)
  
  Timers:CreateTimer(2, function()
    GameRules.StartMovement(entity, 4)
  end)

  -- Create Enchantress item
  local newItem = CreateItem("item_veil_datadriven", nil, nil)
  CreateItemOnPositionForLaunch(entity:GetOrigin(), newItem)
  newItem:LaunchLoot(false, 200, 0.75, entity:GetOrigin() + RandomVector(RandomFloat(20, 100)))

  -- Show some dialog
  CustomGameEventManager:Send_ServerToAllClients(
    "start_speech",
    {type = "jumo_won", player = -1}
  )

  return -1
end