function OnFinishInteraction(eventSourceIndex, args)
  local ent = PlayerResource:GetSelectedHeroEntity(args.PlayerID)

  if not ent.curInteraction then
    return print("shit's wrong with OnFinishInteraction: " .. args.PlayerID)
  end

  ent.curInteraction:finish(args)
end

function OnUnequipItem(eventSourceIndex, args)
  GameRules.UnequipItem(PlayerResource:GetSelectedHeroEntity(args.PlayerID), args.pos, false)
end

function OnSpeechChoice(eventSourceIndex, args)
  CustomGameEventManager:Send_ServerToAllClients(
    "speech_choice",
    {num = args.num}
  )
end

function OnToggleSound(eventSourceIndex, args)
  local entity = PlayerResource:GetSelectedHeroEntity(args.PlayerID)
  if not entity or not entity.curSound then return end

  entity.curSound.mute = not entity.curSound.mute

  if entity.curSound.mute then
    StopSoundEvent(entity.curSound.name, entity)
  else
    StartSoundEvent(entity.curSound.name, entity)
  end
end