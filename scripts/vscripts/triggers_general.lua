function OnHurtZone(trigger)
  local bool = true
  if thisEntity:Attribute_GetIntValue("lethal", 0) == 0 then bool = false end

  EmitSoundOn("Hero_DragonKnight.ProjectileImpact", trigger.activator)

  trigger.activator:ModifyHealth(
    trigger.activator:GetHealth() - thisEntity:Attribute_GetIntValue("damage", 10),
    nil, 
    bool,
    0
  )
end

function ChangeAmbientSound(trigger)
  SoundManager:StartSound(trigger.activator, "DotaTatics." .. thisEntity:GetName(), true, true)
end

function PreviousAmbientSound(trigger)
  SoundManager:PreviousAmbientSound(trigger.activator)
end