function OnCombatZone( trigger )
  if trigger.activator:GetPlayerOwnerID() == -1 then return end

  print("enter " .. thisEntity:GetName())

  GameRules.combatZone[trigger.activator:entindex()] = {
    name = thisEntity:GetName(),
    entity = trigger.activator,
    pos = trigger.activator:GetAbsOrigin()
  }

  Timers:CreateTimer(CheckCombat)
end

function LeaveCombatZone( trigger )
  if trigger.activator:GetPlayerOwnerID() == -1 then return end

  print("left " .. thisEntity:GetName())
  if GameRules.combatZone[trigger.activator:entindex()] and
     GameRules.combatZone[trigger.activator:entindex()].name == thisEntity:GetName() then
    GameRules.combatZone[trigger.activator:entindex()] = nil;
  end
end

function CheckCombat()
  if GameRules.inCombat or GameRules.inInteraction then return end
  if GameRules.combatCooldown > 0 then return GameRules.combatCooldown end

  local aux = false
  for k,v in pairs(GameRules.combatZone) do
    aux = true
    if v.entity:IsNull() then
      GameRules.combatZone[k] = nil
    else
      local dis = (v.pos - v.entity:GetAbsOrigin()):Length2D()
      v.pos = v.entity:GetAbsOrigin()

      if dis > 0 and RandomFloat(0, 1) < (GameRules.encounters[v.name].rate - v.entity.encounterRate) then
        return GameRules.CombatManager:CreateCombat(EntIndexToHScript(k), v.name, true)
      end
    end
  end

  if not aux then return end
  return 0.5
end

function FixedCombat(trigger)
  local entity = trigger.activator

  if thisEntity:Attribute_GetIntValue("slain", 0) > 0 then return end
  if GameRules.inCombat then return end
  if GameRules.combatCooldown > 0 then return GameRules.combatCooldown end

  thisEntity:Attribute_SetIntValue("slain", 1)

  GameRules.CombatManager:CreateCombat(entity, thisEntity:GetName(), false)

  if thisEntity:Attribute_GetIntValue("defaultInteraction", 0) == 1 then
    entity.curInteraction = require("interactions/fixedCombat")
  else
    entity.curInteraction = require("interactions/" .. thisEntity:GetName())
  end
  
  entity.curInteraction = entity.curInteraction:start(entity, thisEntity)
end