if GameRules.Overseer == nil then
  GameRules.Overseer = {
    abilities = {},
    unit = CreateUnitByName( "npc_dota_action_overseer", Vector(0,0,0), true, nil, nil, DOTA_TEAM_GOODGUYS),
    modifiers = {
      action = "modifier_has_action_datadriven",
      move = "modifier_can_move_datadriven",
    }
  }

  -- Combat control
  GameRules.Overseer.abilities.action = GameRules.Overseer.unit:FindAbilityByName("dota_ability_has_action")
  GameRules.Overseer.abilities.move = GameRules.Overseer.unit:FindAbilityByName("dota_ability_can_move")
  GameRules.Overseer.abilities.silence = GameRules.Overseer.unit:FindAbilityByName("dota_ability_cannot_cast")
  GameRules.Overseer.abilities.field = GameRules.Overseer.unit:FindAbilityByName("disruptor_mega_field")
  GameRules.Overseer.abilities.defend = GameRules.Overseer.unit:FindAbilityByName("dota_ability_defend")

  -- Animation control
  GameRules.Overseer.abilities.attackAnimation = GameRules.Overseer.unit:FindAbilityByName("dota_ability_attack_animation")
  GameRules.Overseer.abilities.idleAnimation = GameRules.Overseer.unit:FindAbilityByName("dota_ability_idle_rare_animation")
  GameRules.Overseer.abilities.walkAnimation = GameRules.Overseer.unit:FindAbilityByName("dota_ability_walk_animation")
  GameRules.Overseer.abilities.purgeAnimation = GameRules.Overseer.unit:FindAbilityByName("dota_ability_purge_animation")

  -- Interactions Spells
  GameRules.Overseer.abilities.dummy = GameRules.Overseer.unit:FindAbilityByName("dota_ability_dummy_recover")
  GameRules.Overseer.abilities.dazzle = GameRules.Overseer.unit:FindAbilityByName("dazzle_ritual_datadriven")

  GameRules.Overseer.__index = GameRules.Overseer
end

function GameRules.Overseer:CastTarget(target, ability)
  ExecuteOrderFromTable({
    queue = true,
    OrderType = DOTA_UNIT_ORDER_CAST_TARGET,
    TargetIndex = target:entindex(),
    UnitIndex = GameRules.Overseer.unit:entindex(),
    AbilityIndex = GameRules.Overseer.abilities[ability]:entindex()
  })
end

function GameRules.Overseer:CastPosition(pos, ability)
  ExecuteOrderFromTable({
    queue = true,
    OrderType = DOTA_UNIT_ORDER_CAST_POSITION,
    UnitIndex = GameRules.Overseer.unit:entindex(),
    Position = pos,
    AbilityIndex = GameRules.Overseer.abilities[ability]:entindex()
  })
end