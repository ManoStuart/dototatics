require ("timers")
require ("actions_modifiers")
require ("queue")
require ("combat_trigger")
require ("config/combat_encounters")

if CombatManager == nil then
  CombatManager = class({})
  GameRules.CombatManager = CombatManager()

  GameRules.combatZone = {}
  GameRules.inCombat = false
  CombatManager.blockEndTurn = false
  GameRules.combatCooldown = 0

  GameRules.activeModifiers = {};
  GameRules.activeModifiersCount = 0;

  GameRules.curEncounter = nil
end

function CombatManager:CreateCombat(entity, type, start)
  print("should have found " .. type)
  if GameRules.inCombat or GameRules.inInteraction then return end

  -- Establish combat center
  GameRules.combatCenter = Entities:FindByName(nil, type .. "_center")
  if not GameRules.combatCenter then GameRules.combatCenter = entity:GetOrigin()
  else GameRules.combatCenter = GameRules.combatCenter:GetOrigin()
  end

  -- Random encounter location
  local points = Entities:FindAllByNameWithin(type .. "_pos", GameRules.combatCenter, 800)
  if #points <= 0 then print("no points found") return 0.5 end

  GameRules.inCombat = true

  -- Reset combat variables
  GameRules.combatStartTime = GameRules:GetGameTime()
  GameRules.encounterTeams = {};
  CombatManager.combatEnded = false
  CombatManager.rewardMods = {expMult = 1, expAdd = 0, apMult = 1, apAdd = 0, goldMult = 1, goldAdd = 0}
  GameRules.queue = List.new()
  AddFOWViewer(DOTA_TEAM_BADGUYS, entity:GetOrigin(), 1500, 9000, false)

  -- Find Player Heroes inside Encounter range
  GameRules.combatUnits = FindUnitsInRadius( DOTA_TEAM_BADGUYS, GameRules.combatCenter, nil, 1000, DOTA_UNIT_TARGET_TEAM_BOTH, DOTA_UNIT_TARGET_ALL, 0, 0, false )

  -- Random encounter Type
  local encounterType = RandomInt(1, #GameRules.encounters[type].vec)
  GameRules.curEncounter = GameRules.encounters[type].vec[encounterType]

  -- Get encounter sound
  local sound = "DotaTatics.Encounter"
  if GameRules.curEncounter.sound == 1 then
    sound = "DotaTatics.BossEncounter"
  elseif GameRules.curEncounter.sound == 2 then
    sound = "DotaTatics.FunnyEncounter"
  end

  -- Stop npc movements
  for _,v in pairs(GameRules.combatUnits) do
    if v:GetPlayerOwnerID() == -1 then
      GameRules.StopMovement(v)
    else
      SoundManager:StartSound(v, sound, true, false)
    end
  end

  -- Summon encounter creatures and add to units vector
  for k,v in pairs(GameRules.curEncounter.units) do
    for _,unitName in pairs(GameRules.curEncounter.units[k]) do
      local rand = RandomInt(1, #points)

      GameRules.combatUnits[#GameRules.combatUnits+1] = CreateUnitByName(unitName, points[rand]:GetOrigin(), true, nil, nil, k)
    end
  end

  for _,v in pairs(GameRules.combatUnits) do
    v:Hold()

    -- Mark unit in combat for AIs
    v.inCombat = true

    -- Get each team's unit count
    local num = v:GetTeamNumber()
    if not GameRules.encounterTeams[num] then GameRules.encounterTeams[num] = 0 end
    GameRules.encounterTeams[num] = GameRules.encounterTeams[num] + 1
    
    -- Add unit to a random position in queue
    GameRules.queue = List.insert(GameRules.queue, v, RandomInt(0, 10))
  end

  -- Create environment creatures
  if (GameRules.curEncounter.ambient) then
    for _,v in pairs(GameRules.curEncounter.ambient) do
      local newEntity = CreateUnitByName(v, GameRules.combatCenter, false, nil, nil, DOTA_TEAM_BADGUYS)
      GameRules.combatUnits[#GameRules.combatUnits+1] = newEntity

      -- Add unit to a random position in queue
      if newEntity.ai then
        GameRules.queue = List.insert(GameRules.queue, newEntity, RandomInt(11, 15))
      end
    end
  end

  -- Set Queue hud
  CustomNetTables:SetTableValue("heap_order", "heap", List.toNettable(GameRules.queue));
  CustomGameEventManager:Send_ServerToAllClients( "start_fade", {encounter = true} )

  -- Start Play Turns
  GameRules.curPhase = 0
  if start then Timers:CreateTimer(5, GameRules.CombatManager.NextTurn) end

  -- Start Restrictions
  Timers:CreateTimer(GameRules.CombatManager.MegaField)
  Timers:CreateTimer(0.1, GameRules.CombatManager.StartRestrictions)
end

function CombatManager:NextTurn()
  if GameRules.queue == nil or CombatManager.combatEnded then return end

  local curEntity = GameRules.queue.value
  local playerId = curEntity:GetPlayerOwnerID()

  -- print("NextTurn " .. curEntity:GetUnitName())

  -- Reset turn variables
  GameRules.actionsMade = nil
  GameRules.curPhase = GameRules.curPhase + 1
  GameRules.turnAP = curEntity.haste
  executeModifiers(curEntity)

  StartUnit(curEntity)

  if playerId ~= -1 then
    -- Set current player HUD
    CustomGameEventManager:Send_ServerToPlayer(
      curEntity:GetPlayerOwner(),
      "start_turn",
      {entity = curEntity:entindex(), player = playerId, isReset = false}
    )

    -- Enforce max turn time
    local num = GameRules.curPhase
    Timers:CreateTimer(30, function()
      if num == GameRules.curPhase then
        print("took too long")
        CombatManager.blockEndTurn = false
        if GameRules.turnAP == 0 then
          GameRules.CombatManager:AddTurnAP(15)
        end

        GameRules.CombatManager:EndTurn(curEntity:entindex())
      end
    end)
  else
    -- Call AI logic
    Timers:CreateTimer(0.5, function()
      curEntity.ai:startTurn()
    end)

    -- Fail safe
    local num = GameRules.curPhase
    Timers:CreateTimer(8, function()
      if num == GameRules.curPhase and not curEntity:IsNull() then
        print("Ai fail safe")
        CombatManager.blockEndTurn = false
        if GameRules.turnAP == 0 then
          GameRules.CombatManager:AddTurnAP(15)
        end

        GameRules.CombatManager:EndTurn(curEntity:entindex())
      end
    end)
  end
end

function CombatManager:EndTurn(id)
  if GameRules.queue == nil or CombatManager.combatEnded or CombatManager.blockEndTurn then return end
  if GameRules.queue.value:entindex() ~= id then return end

  local entity = GameRules.queue.value

  StopUnit(entity)

  -- Clear Actions HUD
  local playerId = entity:GetPlayerOwnerID()
  if playerId ~= -1 then
    CustomGameEventManager:Send_ServerToPlayer(
      entity:GetPlayerOwner(),
      "finish_turn",
      {entity = entity:entindex(), player = playerId}
    )
  end

  local key = GameRules.queue.key;
  GameRules.queue = List.changeKey(GameRules.queue, key, key + GameRules.turnAP)

  -- Set Queue hud
  CustomNetTables:SetTableValue("heap_order", "heap", List.toNettable(GameRules.queue));

  Timers:CreateTimer(0.7, function()
    GameRules.CombatManager:NextTurn()
  end)
end

function CombatManager:FinishCombat(winnerTeam, aliveCreatures)
  if winnerTeam == -1 then return print("shits fucked up: FinishCombat") end

  -- Calculate Encounter rewards
  local mod = CombatManager.rewardMods
  local hudInfo = {
    won = 0,
    time = GameRules:GetGameTime() - GameRules.combatStartTime,
    exp = math.ceil((GameRules.curEncounter.exp + mod.expAdd) * mod.expMult / aliveCreatures),
    gold = math.ceil((GameRules.curEncounter.gold + mod.goldAdd) * mod.goldMult / aliveCreatures),
    ap = math.ceil((GameRules.curEncounter.ap + mod.apAdd) * mod.apMult)
  }

  -- Set End Encounter HUD
  if winnerTeam == DOTA_TEAM_GOODGUYS then hudInfo.won = 1 end
  CustomGameEventManager:Send_ServerToAllClients("end_encounter", hudInfo)

  local array = List.GetEntities(GameRules.queue)
  GameRules.queue = nil

  -- Reenable Combat Encounters
  GameRules.combatCooldown = GameRules.curEncounter.cd
  Timers:CreateTimer(0.1, GameRules.CombatManager.RemoveCombatCooldown)

  local pos = Entities:FindByClassname(nil, "info_player_start_goodguys")

  for _,v in pairs(GameRules.combatUnits) do
    -- Remove units from combat (AI)
    v.inCombat = false

    if not v:IsNull() then
      if v:GetPlayerOwnerID() == -1 then
        if v:GetTeam() == DOTA_TEAM_GOODGUYS and v:IsAlive() then
          -- Remove restrictions
          RemoveRestrictions(v)

          -- Restart npc movements
          GameRules.StartMovement(v, 1)
        else
          print("Destroying " .. v:GetUnitName())
          -- Remove encounter creeps
          v:ForceKill(false)
          v:Destroy()
        end
      else
        -- Giff xp n gold
        if v:IsAlive() and v:GetTeamNumber() == winnerTeam then
          RewardUnit(v, hudInfo.exp, hudInfo.gold, hudInfo.ap)
        else
          SoundManager:StartSound(v, "DotaTatics.GameOver", false, false)

          -- Revive plebs
          Timers:CreateTimer(0.5, function() v:RespawnHero(false, false, false) end)

          -- Reset dead faggot to main city
          Timers:CreateTimer(0.7, function()
            FindClearSpaceForUnit(v, pos:GetOrigin(), true)
          end)
        end
        
        -- Remove restrictions
        RemoveRestrictions(v)
      end

      -- Call fixed combat callback
      if v.curInteraction then v.curInteraction:finishCombat(v:GetTeamNumber() == winnerTeam) end
    end
  end

  CombatManager:resetModifiers()

  CombatManager:ReenableAll()
end

function RewardUnit(entity, exp, gold, ap)
  SoundManager:StartSound(entity, "DotaTatics.Victory", false, false)

  -- Experience
  entity:AddExperience(exp, 0, false, true)
  entity:SetAbilityPoints(0)

  -- Gold
  entity:ModifyGold(gold, true, 0)

  -- Ability Points
  for _,v in pairs(entity.equip) do
    if v.ap then
      local level = v.ap / 100
      v.ap = v.ap + ap

      if level < v.ap / 100 then
        local ability = entity:FindAbilityByName(v.ability)
        local max = ability:GetMaxLevel()
        level = v.ap / 100

        ability:SetLevel(level <= max and level or max)
      end
    end
  end
end

---------------------------------------------------------
--                      CALLBACKS
---------------------------------------------------------

function CombatManager:FinishAction(entity, type, ap)
  GameRules.CombatManager:AddTurnAP(ap)

  if GameRules.actionsMade ~= nil then
    if type ~= "move" and GameRules.actionsMade ~= "move" then
      return print("shit happened FinishAction")
    end

    Timers:CreateTimer(0.5, function()
      GameRules.CombatManager:EndTurn(entity:entindex())
    end)
  end

  GameRules.actionsMade = type;
end

function CombatManager:UnitKilled(entity)
  if not GameRules.inCombat or not GameRules.queue then return end
  
  -- Damage over time killed the fucker.
  if GameRules.queue.value == entity then
    GameRules.CombatManager:EndTurn(entity:entindex())
  end

  local id = entity:entindex()
  GameRules.queue = List.remove(GameRules.queue, entity.queueKey)

  CustomGameEventManager:Send_ServerToAllClients("entity_killed", {entity = id})

  local num = entity:GetTeamNumber()
  GameRules.encounterTeams[num] = GameRules.encounterTeams[num] - 1

  CombatManager:CheckEndGame()
end

function CombatManager:OnFinishTurn( eventSourceIndex, args )
  if not GameRules.queue then return end

  if (eventSourceIndex.PlayerID ~= GameRules.queue.value:GetPlayerOwnerID()) then
    return print("shit is wrong OnFinishTurn")
  end

  -- Unit used defend
  if (GameRules.actionsMade == "move" or GameRules.actionsMade == nil) then
    GameRules.Overseer:CastTarget(GameRules.queue.value, "defend")

    GameRules.CombatManager:AddTurnAP(10)
  end

  GameRules.CombatManager:EndTurn(GameRules.queue.value:entindex())
end

---------------------------------------------------------
--                  MODIFIERS LOGIC
---------------------------------------------------------

function CombatManager:registerModifier(obj)
  GameRules.activeModifiers[GameRules.activeModifiersCount+1] = obj
  GameRules.activeModifiersCount = GameRules.activeModifiersCount + 1
end

function CombatManager:resetModifiers()
  for k,v in pairs(GameRules.activeModifiers) do
    if v.remove(entity) then
      GameRules.activeModifiers[k] = nil
    end
  end
end

function executeModifiers(entity)
  for k,v in pairs(GameRules.activeModifiers) do
    if v.turn(entity) then
      GameRules.activeModifiers[k] = nil
    end
  end
end

---------------------------------------------------------
--                        UTILS
---------------------------------------------------------

function CombatManager:CheckEndGame()
  if CombatManager.combatEnded then return end

  local aliveTeams = 0
  local winnerTeam = -1
  local aliveCreatures = 0

  for k,v in pairs(GameRules.encounterTeams) do
    if v > 0 then
      aliveTeams = aliveTeams + 1
      winnerTeam = k
      aliveCreatures = aliveCreatures + v
    end
  end

  if aliveTeams <= 1 then
    CombatManager.combatEnded = true
    Timers:CreateTimer(2, function()
      CombatManager:FinishCombat(winnerTeam, aliveCreatures)
    end)
  end
end

function CombatManager:UpdateQueueHUD()
  CustomNetTables:SetTableValue("heap_order", "heap", List.toNettable(GameRules.queue));
end

function CombatManager:AddTurnAP(num)
  GameRules.turnAP = GameRules.turnAP + num
end


function CombatManager:UndoFailedAction(entity, ap)
  if CombatManager.combatEnded then return end

  CombatManager.blockEndTurn = true

  entity:Hold()
  CombatManager:AddTurnAP(-ap)

  Timers:CreateTimer(1, function()
    GameRules.actionsMade = nil
    CombatManager.blockEndTurn = false
    GameRules.Overseer:CastTarget(entity, "action")

    local playerId = entity:GetPlayerOwnerID()
    if playerId ~= -1 then
      -- Set current player HUD
      CustomGameEventManager:Send_ServerToPlayer(
        entity:GetPlayerOwner(),
        "start_turn",
        {entity = entity:entindex(), player = playerId, isReset = true}
      )
    end
  end)
end

function CombatManager:ChangeRewards(expAdd, expMult, goldAdd, goldMult, apAdd, apMult)
  CombatManager.rewardMods.expAdd = CombatManager.rewardMods.expAdd + expAdd
  CombatManager.rewardMods.expMult = CombatManager.rewardMods.expMult + expMult
  CombatManager.rewardMods.goldAdd = CombatManager.rewardMods.goldAdd + goldAdd
  CombatManager.rewardMods.goldMult = CombatManager.rewardMods.goldMult + goldMult
  CombatManager.rewardMods.apAdd = CombatManager.rewardMods.apAdd + apAdd
  CombatManager.rewardMods.apMult = CombatManager.rewardMods.apMult + apMult
end

function CombatManager:AddNewUnit(entity, real)
  if real then
    -- Get each team's unit count
    local num = entity:GetTeamNumber()
    if not GameRules.encounterTeams[num] then GameRules.encounterTeams[num] = 0 end
    GameRules.encounterTeams[num] = GameRules.encounterTeams[num] + 1
  end
  
  -- Add unit to a random position in queue
  local key = GameRules.queue.key
  GameRules.queue = List.insert(GameRules.queue, entity, RandomInt(key, key + 15))

  -- Init units restrictions
  AddRestrictions(entity)

  -- Keep unit in combat array
  GameRules.combatUnits[#GameRules.combatUnits+1] = entity
end

function CombatManager:ChangeTeams(entity, team)
  local num = entity:GetTeamNumber()

  -- Remove from previous team
  GameRules.encounterTeams[num] = GameRules.encounterTeams[num] - 1

  -- Add to new team
  if not GameRules.encounterTeams[team] then GameRules.encounterTeams[team] = 0 end
  GameRules.encounterTeams[team] = GameRules.encounterTeams[team] + 1

  entity:SetTeam(team)

  CombatManager:CheckEndGame()
end

function CombatManager:StartRestrictions()
  -- Init units restrictions
  for _,v in pairs(GameRules.combatUnits) do
    AddRestrictions(v)
  end

  CombatManager:DisableAll(true)
end

function CombatManager:DisableAll(isCombat)
  if isCombat then
    GameRules.inCombat = true
  else
    GameRules.inInteraction = true
  end

  -- Make sure every hero is disabled
  for _,v in pairs(HeroList:GetAllHeroes()) do
    AddRestrictions(v)
  end
end

function CombatManager:ReenableAll()
  GameRules.inCombat = false
  GameRules.inInteraction = false

  Timers:CreateTimer(GameRules.combatCooldown, CheckCombat)
  
  -- Make sure every hero is reenabled
  for _,v in pairs(HeroList:GetAllHeroes()) do
    RemoveRestrictions(v)
  end
end

---------------------------------------------------------
--                       THINKERS
---------------------------------------------------------

function CombatManager:MegaField()
  if not GameRules.inCombat then return end

  GameRules.Overseer:CastPosition(GameRules.combatCenter, "field")
  AddFOWViewer(DOTA_TEAM_GOODGUYS, GameRules.combatCenter, 1100, 10, false)

  return 9
end

function CombatManager:RemoveCombatCooldown()
  GameRules.combatCooldown = GameRules.combatCooldown - 0.1

  if GameRules.combatCooldown <= 0 then return end

  return 0.1
end