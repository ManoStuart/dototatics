function ModifierInit(keys)
  local entity = keys.target
  local ability = keys.ability
  local modifier = keys.modifier
  local level = (ability:GetLevel() - 1)
  local duration = ability:GetLevelSpecialValueFor("duration", level)

  entity:SetModifierStackCount(modifier, ability, duration)

  local damageTable = {
    victim = entity,
    attacker = keys.caster,
    damage = ability:GetLevelSpecialValueFor("damage", level),
    damage_type = ability:GetAbilityDamageType()
  }

  if keys.heal then
    damageTable.damage = ability:GetLevelSpecialValueFor("heal", level)
  end

  local obj = {
    turn = function (unit)
      if entity:IsNull() then return true end
      if unit:entindex() == entity:entindex() then
        if not entity:FindModifierByName(modifier) then
          return true
        end

        if keys.heal then
          entity:Heal(damageTable.damage, damageTable.attacker)
        else
          ApplyDamage(damageTable)
        end

        -- Get the current stack count
        local current = entity:GetModifierStackCount(modifier, ability)

        -- If its 1 then remove the modifier entirely, otherwise just reduce the stack number by 1
        if current <= 1 then
          entity:RemoveModifierByName(modifier)
          return true
        else
          entity:SetModifierStackCount(modifier, ability, current - 1)
        end
      end

      return false
    end,

    remove = function (unit)
      if entity:IsNull() then return true end
      entity:RemoveModifierByName(keys.modifier)
      return true
    end
  }

  GameRules.CombatManager:registerModifier(obj)
end

function RefreshStacks(keys)
  local entity = keys.target
  local ability = keys.ability
  local modifier = keys.modifier
  local duration = ability:GetLevelSpecialValueFor("duration", (ability:GetLevel() - 1))

  if entity:FindModifierByName(modifier) then
    entity:SetModifierStackCount(modifier, ability, duration)
  end
end

function AuraInit(keys)
  local entity = keys.target
  local ability = keys.ability
  local level = ability:GetLevel() - 1

  local damageTable = {
    victim = entity,
    attacker = keys.caster,
    damage = ability:GetLevelSpecialValueFor("damage", level),
    damage_type = ability:GetAbilityDamageType()
  }

  if keys.heal then
    damageTable.damage = ability:GetLevelSpecialValueFor("heal", level)
  end
  
  local obj = {
    turn = function (unit)
      if entity:IsNull() then return true end
      if ability:IsNull() then
        entity:RemoveModifierByName(keys.modifier)
        return true
      end
      
      if unit:entindex() == entity:entindex() then
        if not entity:FindModifierByName(keys.modifier) then return true end

        if keys.heal then
          entity:Heal(damageTable.damage, damageTable.attacker)
        else
          ApplyDamage(damageTable)
        end
      end

      return false
    end,

    remove = function (unit)
      if entity:IsNull() then return true end
      return false
    end,
  }

  GameRules.CombatManager:registerModifier(obj)
end