function ModifierInit(keys)
  local pos = keys.caster:GetAbsOrigin()
  local team = keys.caster:GetTeamNumber()
  local ability = keys.ability
  local level = ability:GetLevel() - 1

  local min = ability:GetLevelSpecialValueFor("duration_min", level)
  local max = ability:GetLevelSpecialValueFor("duration_max", level)
  local duration = RandomInt(min, max)

  local curTurn = 0
  local obj = {
    turn = function (unit)
      curTurn = curTurn + 1
      if curTurn >= duration then
        local entity = CreateUnitByName(keys.unitName, pos, true, nil, nil, team)

        CombatManager:AddNewUnit(entity, 1)
        CombatManager:UpdateQueueHUD()

        return true
      end
      return false
    end,

    remove = function (unit)
      return true
    end
  }

  GameRules.CombatManager:registerModifier(obj)
end