function Initialize( keys )
  local caster = keys.caster
  local target = keys.target
  local ability = keys.ability

  local max_stacks = ability:GetLevelSpecialValueFor("charges", ability:GetLevel() - 1)
  target:SetModifierStackCount(keys.modifier, ability, max_stacks)
end

function Decrement( keys )
  local caster = keys.caster
  local target = keys.attacker
  local ability = keys.ability
  local modifier = keys.modifier

  -- Get the current stack count
  local current = target:GetModifierStackCount(modifier, ability)

  -- If its 1 then remove the modifier entirely, otherwise just reduce the stack number by 1
  if current <= 1 then
    target:RemoveModifierByNameAndCaster(modifier, caster)
  else
    target:SetModifierStackCount(modifier, ability, current - 1)
  end
end