function ModifierInit(keys)
  local entity = keys.target
  local ability = keys.ability
  local modifier = keys.modifier
  local duration = ability:GetLevelSpecialValueFor("duration", (ability:GetLevel() - 1))
  local haste = ability:GetLevelSpecialValueFor("haste", (ability:GetLevel() - 1))

  entity.haste = entity.haste + haste
  entity:SetModifierStackCount(modifier, ability, duration)

  local obj = {
    turn = function (unit)
      if entity:IsNull() then return true end
      if unit:entindex() == entity:entindex() then
        if not entity:FindModifierByName(modifier) then
          return true
        end

        -- Get the current stack count
        local current = entity:GetModifierStackCount(modifier, ability)

        -- If its 1 then remove the modifier entirely, otherwise just reduce the stack number by 1
        if current <= 1 then
          entity:RemoveModifierByName(modifier)
          entity.haste = entity.haste - haste
          return true
        else
          entity:SetModifierStackCount(modifier, ability, current - 1)
        end
      end

      return false
    end,

    remove = function (unit)
      if entity:IsNull() then return true end
      entity.haste = entity.haste - haste
      entity:RemoveModifierByName(modifier)
      return true
    end
  }

  GameRules.CombatManager:registerModifier(obj)
end

function RefreshStacks(keys)
  local entity = keys.target
  local ability = keys.ability
  local modifier = keys.modifier
  local duration = ability:GetLevelSpecialValueFor("duration", (ability:GetLevel() - 1))

  if entity:FindModifierByName(modifier) then
    entity:SetModifierStackCount(modifier, ability, duration)
  end
end

function AuraInit(keys)
  local entity = keys.target
  local ability = keys.ability

  entity.haste = entity.haste + ability:GetLevelSpecialValueFor("haste", (ability:GetLevel() - 1))
end

function AuraRemove(keys)
  local entity = keys.target
  local ability = keys.ability

  entity.haste = entity.haste - ability:GetLevelSpecialValueFor("haste", (ability:GetLevel() - 1))
end