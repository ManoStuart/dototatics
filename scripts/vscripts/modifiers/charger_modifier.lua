function ModifierInit(keys)
  local entity = keys.target
  local caster = keys.caster
  local ability = keys.ability
  local modifier = keys.modifier
  local target = keys.targetModifier 
  local duration = ability:GetLevelSpecialValueFor("duration", (ability:GetLevel() - 1))
  local max = ability:GetLevelSpecialValueFor("max_charges", (ability:GetLevel() - 1))

  entity:SetModifierStackCount(modifier, ability, duration)

  local obj = {
    turn = function (unit)
      if entity:IsNull() then return true end
      if ability:IsNull() and keys.permanent then
        entity:RemoveModifierByName(modifier)
        return true
      end

      if unit:entindex() == entity:entindex() then
        if not entity:FindModifierByName(modifier) then
          return true
        end

        if #(entity:FindAllModifiersByName(target)) >= max then return end

        -- Get the current stack count
        local current = entity:GetModifierStackCount(modifier, ability)

        -- If its 1 then remove the modifier entirely, otherwise just reduce the stack number by 1
        if current <= 1 then
          entity:SetModifierStackCount(modifier, ability, duration + 1)
          ability:ApplyDataDrivenModifier(caster, entity, target, {})
        else
          entity:SetModifierStackCount(modifier, ability, current - 1)
        end
      end

      return false
    end,

    remove = function (unit)
      if entity:IsNull() then return true end
      if keys.permanent then return false end

      entity:RemoveModifierByName(modifier)
      if keys.wipe then entity:RemoveModifierByName(target) end

      return true
    end
  }

  GameRules.CombatManager:registerModifier(obj)
end

function RefreshStacks(keys)
  local entity = keys.target
  local ability = keys.ability
  local modifier = keys.modifier
  local duration = ability:GetLevelSpecialValueFor("duration", (ability:GetLevel() - 1))

  if entity:FindModifierByName(modifier) then
    entity:SetModifierStackCount(modifier, ability, duration)
  end
end