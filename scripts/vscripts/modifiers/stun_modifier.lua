function ModifierInit(keys)
  local owner = keys.caster
  local entity = keys.target
  local ability = keys.ability
  local turnCount = 0

  local stun
  if ability then
    stun = ability:GetLevelSpecialValueFor("stun_duration", (ability:GetLevel() - 1))
  else
    stun = keys.stun
  end

  GameRules.queue = List.changeKey(GameRules.queue, entity.queueKey, entity.queueKey + stun)

  entity:AddNewModifier(owner, ability, "modifier_stunned", {})

  local obj = {
    turn = function (unit)
      if entity:IsNull() then return end

      if unit:entindex() == entity:entindex() then
        entity:RemoveModifierByName("modifier_stunned")
        return true
      end

      return false
    end,

    remove = function (unit)
      if entity:IsNull() then return true end
      entity:RemoveModifierByName("modifier_stunned")
      
      return true
    end
  }

  GameRules.CombatManager:registerModifier(obj)
end