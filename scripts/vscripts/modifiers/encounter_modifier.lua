function Start(keys)
  local caster = keys.caster
  caster.encounterRate = caster.encounterRate + keys.rate
end

function End(keys)
  local caster = keys.caster
  caster.encounterRate = caster.encounterRate - keys.rate
end