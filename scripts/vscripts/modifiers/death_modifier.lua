function ModifierInit(keys)
  local entity = keys.target
  local ability = keys.ability
  local modifier = keys.modifier
  local duration = ability:GetLevelSpecialValueFor("duration", (ability:GetLevel() - 1))

  entity:SetModifierStackCount(modifier, ability, duration)

  local obj = {
    turn = function (unit)
      if entity:IsNull() then return true end
      if unit:entindex() == entity:entindex() or keys.always then
        if not entity:FindModifierByName(modifier) then
          return true
        end

        -- Get the current stack count
        local current = entity:GetModifierStackCount(modifier, ability)

        -- If its 1 then remove the modifier entirely, otherwise just reduce the stack number by 1
        if (current <= 1 and not backwards) or (current >= duration and backwards) then
          entity:RemoveModifierByName(modifier)
          entity:Kill(ability, keys.caster)
          return true
        else
          entity:SetModifierStackCount(modifier, ability, current + (backwards and 1 or -1))
        end
      end

      return false
    end,

    remove = function (unit)
      if entity:IsNull() then return true end
      if keys.unavoidable then entity:Kill(ability, keys.caster) end
      entity:RemoveModifierByName(keys.modifier)
      return true
    end
  }

  GameRules.CombatManager:registerModifier(obj)
end

function RefreshStacks(keys)
  local entity = keys.target
  local ability = keys.ability
  local modifier = keys.modifier
  local duration = ability:GetLevelSpecialValueFor("duration", (ability:GetLevel() - 1))

  if entity:FindModifierByName(modifier) then
    entity:SetModifierStackCount(modifier, ability, duration)
  end
end