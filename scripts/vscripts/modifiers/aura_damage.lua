function ModifierInit(keys)
  local owner = keys.caster
  local ability = keys.ability

  local curTurn = 0
  local duration = ability:GetLevelSpecialValueFor("duration", (ability:GetLevel() - 1))
  local damage = ability:GetLevelSpecialValueFor("damage", (ability:GetLevel() - 1))
  local damageType = ability:GetAbilityDamageType()

  local thinker = ability:ApplyDataDrivenThinker(owner, keys.target_points[1], keys.thinker, {})

  EmitSoundOnLocationWithCaster(keys.target_points[1], keys.sound, owner)

  local obj = {
    turn = function (unit)
      if unit:HasModifier(keys.modifier) then
        local damageTable = {victim = unit, attacker = owner, damage = damage, damage_type = damageType}
        ApplyDamage(damageTable)
      end

      curTurn = curTurn + 1
      if curTurn >= duration then
        thinker:RemoveSelf()
        StopSoundOn(keys.sound, owner)
        return true
      end
      return false
    end,

    remove = function (unit)
      thinker:RemoveSelf()
      StopSoundOn(keys.sound, owner)
      return true
    end
  }

  GameRules.CombatManager:registerModifier(obj)
end