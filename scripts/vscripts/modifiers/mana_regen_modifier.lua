function ModifierInit(keys)
  local entity = keys.target
  local ability = keys.ability
  local modifier = keys.modifier
  local level = (ability:GetLevel() - 1)

  local duration = ability:GetLevelSpecialValueFor("duration", level)
  local mana = ability:GetLevelSpecialValueFor("mana_regen", level)

  entity:SetModifierStackCount(modifier, ability, duration)

  local obj = {
    turn = function (unit)
      if entity:IsNull() then return true end
      if unit:entindex() == entity:entindex() then
        if not entity:FindModifierByName(modifier) then
          return true
        end

        if keys.drain then
          entity:ReduceMana(mana)
        else
          entity:GiveMana(mana)
        end

        -- Get the current stack count
        local current = entity:GetModifierStackCount(modifier, ability)

        -- If its 1 then remove the modifier entirely, otherwise just reduce the stack number by 1
        if current <= 1 then
          entity:RemoveModifierByName(modifier)
          return true
        else
          entity:SetModifierStackCount(modifier, ability, current - 1)
        end
      end

      return false
    end,

    remove = function (unit)
      if entity:IsNull() then return true end
      entity:RemoveModifierByName(keys.modifier)
      return true
    end
  }

  GameRules.CombatManager:registerModifier(obj)
end

function RefreshStacks(keys)
  local entity = keys.target
  local ability = keys.ability
  local modifier = keys.modifier
  local duration = ability:GetLevelSpecialValueFor("duration", (ability:GetLevel() - 1))

  if entity:FindModifierByName(modifier) then
    entity:SetModifierStackCount(modifier, ability, duration)
  end
end

function AuraInit(keys)
  local entity = keys.target
  local ability = keys.ability
  local mana = ability:GetLevelSpecialValueFor("mana_regen", (ability:GetLevel() - 1))
  
  local obj = {
    turn = function (unit)
      if entity:IsNull() then return true end
      if ability:IsNull() then
        entity:RemoveModifierByName(keys.modifier)
        return true
      end

      if unit:entindex() == entity:entindex() then
        if not entity:FindModifierByName(keys.modifier) then return true end

        if keys.drain then
          entity:ReduceMana(mana)
        else
          entity:GiveMana(mana)
        end
      end

      return false
    end,

    remove = function (unit)
      if entity:IsNull() then return true end
      return false
    end,
  }

  GameRules.CombatManager:registerModifier(obj)
end