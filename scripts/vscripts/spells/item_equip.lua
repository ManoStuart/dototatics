function ModifierInit(keys)
  local entity = keys.caster
  local ability = keys.ability

  if keys.hasRange and entity:FindModifierByName("modifier_item_range") then
    entity:RemoveModifierByName("modifier_item_range")
  end

  -- 0: Weapon, 1: Offhand, 2: Armor, 3: Boots, 4: Head, 5: Trinket
  local slot = ability:GetLevelSpecialValueFor("slot", (ability:GetLevel() - 1))
  -- 0: Can go on offhand, 1: 1 hand, 2: 2 hand
  local size = ability:GetLevelSpecialValueFor("weapon_size", (ability:GetLevel() - 1))
  local capability = ability:GetLevelSpecialValueFor("atk_capability", (ability:GetLevel() - 1))
  local speed = ability:GetLevelSpecialValueFor("atk_speed", (ability:GetLevel() - 1))

  local targetSlot = slot

  -- Equip on offhand
  if slot == 0 and size == 0 and not entity.equip[1] and entity.equip[0] then targetSlot = 1

  -- Equipping offhand only when 2 handed
  elseif slot == 1 and entity.equip[1] and entity.equip[1].modifier == nil then
    GameRules.UnequipItem(entity, 0, false)
    GameRules.UnequipItem(entity, 1, false)
    GameRules.Equip(entity, nil, 0, false)

  -- Unequip current main weapon
  elseif entity.equip[slot] then GameRules.UnequipItem(entity, slot, false) end

  -- Equiped weapon is 2 handed
  if slot == 0 and size == 2 then
    GameRules.UnequipItem(entity, 1, false)
    entity.equip[1] = {modifier = nil, item = nil, atkSpeed = 0, ability = nil, ap = nil, rpg = nil}
    GameRules.Equip(entity, ability:GetName(), 1, true)
  end

  -- Unequip dummy weapon from offhand
  if slot == 0 and size ~= 2 and entity.equip[1] and entity.equip[1].modifier == nil then
    GameRules.UnequipItem(entity, 1, false)
    GameRules.Equip(entity, nil, 1, false)
  end

  -- Change model, projectile and attack capability
  if targetSlot == 0 then
    local model;
    if capability == 1 then
      model = "models/creeps/lane_creeps/creep_radiant_melee/radiant_melee.vmdl"
      entity:SetRangedProjectileName("")
    elseif capability == 2 then
      model = "models/creeps/lane_creeps/creep_radiant_ranged/radiant_ranged.vmdl"
      entity:SetRangedProjectileName(keys.projectile)
    else
      entity:SetRangedProjectileName("")
    end
    
    if entity:GetUnitName() == "npc_dota_hero_jakiro" then entity:SetOriginalModel(model) end

    entity.atkType = capability
    if not GameRules.inCombat then entity:SetAttackCapability(capability) end
  end

  -- Register new item
  entity.equip[targetSlot] = {
    item = ability:GetName(),
    modifier = keys.modifier,
    atkSpeed = speed,
    ability = keys.newAbility,
    ap = keys.newAbility and (ability.ap or 100) or nil,
    rpg = ability.rpg
  }

  -- Offhand settings
  if targetSlot == 1 and slot == 0 then
    entity.equip[targetSlot].atkSpeed = speed/2
    entity.equip[targetSlot].weapon = 1

    if entity.equip[0].modifier == keys.modifier then
      entity:SetModifierStackCount(keys.modifier, ability, 2)
    end
  end
  if slot == 0 then entity.equip[targetSlot].stackable = 1 end

  -- Calculate new atkSpeed
  entity.atkSpeed = 0
  for _,v in pairs(entity.equip) do
    entity.atkSpeed = entity.atkSpeed + v.atkSpeed
  end

  -- Equip new Ability
  if keys.newAbility then
    entity:AddAbility(keys.newAbility)
    local newAbility = entity:FindAbilityByName(keys.newAbility)

    newAbility:SetLevel(entity.equip[targetSlot].ap / 100)

    local type = newAbility:GetLevelSpecialValueFor("encounter_only", 0)
    if type == -1 or (type == 0 and not GameRules.inCombat) or (type == 1 and GameRules.inCombat) then
      newAbility:SetHidden(true)
    end
  end

  -- Update HUD
  GameRules.Equip(entity, ability:GetName(), targetSlot, false)

  -- Remove item after usage
  entity:RemoveItem(ability)
end

function Consumable(keys)
  keys.ability:EndCooldown()
end