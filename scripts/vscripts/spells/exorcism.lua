function ExorcismStart( event )
  local caster = event.caster
  local ability = event.ability
  local radius = ability:GetLevelSpecialValueFor( "radius", ability:GetLevel() - 1 )
  local spirits = ability:GetLevelSpecialValueFor( "spirits", ability:GetLevel() - 1 )
  local delay_between_spirits = ability:GetLevelSpecialValueFor( "delay_between_spirits", ability:GetLevel() - 1 )
  local unit_name = "npc_dota_environment_spirits"

  -- Initialize the table to keep track of all spirits
  caster.spirits = {}
  for i=1,spirits do
    Timers:CreateTimer(i * delay_between_spirits, function()
      local unit = CreateUnitByName(unit_name, caster:GetAbsOrigin(), true, caster, caster, caster:GetTeamNumber())

      -- The modifier takes care of the physics and logic
      ability:ApplyDataDrivenModifier(caster, unit, "modifier_exorcism_spirit", {})
      
      -- Add the spawned unit to the table
      table.insert(caster.spirits, unit)
    end)
  end
end

function ExorcismPhysics( event )
  local caster = event.caster
  local unit = event.target
  local ability = event.ability
  local level = ability:GetLevel() - 1
  local radius = ability:GetLevelSpecialValueFor( "radius", level )
  local spirit_speed = ability:GetLevelSpecialValueFor( "spirit_speed", level )
  local damage = ability:GetLevelSpecialValueFor( "damage", level )
  local max_distance = ability:GetLevelSpecialValueFor( "max_distance", level )
  local abilityDamageType = ability:GetAbilityDamageType()
  local abilityTargetType = ability:GetAbilityTargetType()
  local particleDamage = "particles/units/heroes/hero_death_prophet/death_prophet_exorcism_attack.vpcf"
  local particleDamageBuilding = "particles/units/heroes/hero_death_prophet/death_prophet_exorcism_attack_building.vpcf"

  -- Make the spirit a physics unit
  -- Physics:Unit(unit)

  -- General properties
  -- unit:PreventDI(true)
  -- unit:SetAutoUnstuck(false)
  -- unit:SetNavCollisionType(PHYSICS_NAV_NOTHING)
  -- unit:FollowNavMesh(false)
  -- unit:SetPhysicsVelocityMax(spirit_speed)
  unit.physicsVelocity = spirit_speed * RandomVector(1)
  -- unit:SetPhysicsFriction(0)
  -- unit:Hibernate(false)
  -- unit:SetGroundBehavior(PHYSICS_GROUND_LOCK)

  -- Initial default state
  unit.state = "acquiring"

  -- Color Debugging for points and paths. Turn it false later!
  local Debug = false
  local pathColor = Vector(255,255,255) -- White to draw path
  local targetColor = Vector(255,0,0) -- Red for enemy targets
  local idleColor = Vector(0,255,0) -- Green for moving to idling points
  local returnColor = Vector(0,0,255) -- Blue for the return
  local endColor = Vector(0,0,0) -- Back when returning to the caster to end
  local draw_duration = 3

  -- Find one target point at random which will be used for the first acquisition.
  unit.targetPos = caster:GetAbsOrigin() + RandomVector(RandomInt(radius/2, radius))
  unit.targetPos.z = GetGroundHeight(unit.targetPos,nil)

  -- This is set to repeat on each frame
  Timers:CreateTimer(0.1, function()
    if unit:IsNull() or caster:IsNull() then return end

    -- Move the unit orientation to adjust the particle
    unit:SetForwardVector( ( unit.physicsVelocity ):Normalized() )

    -- Current positions
    local source = caster:GetAbsOrigin()
    local current_position = unit:GetAbsOrigin()

    -- Print the path on Debug mode
    if Debug then DebugDrawCircle(current_position, pathColor, 0, 2, true, draw_duration) end

    local enemies = nil

    -- MOVEMENT 
    -- Get the direction
    local diff = unit.targetPos - unit:GetAbsOrigin()
    diff.z = 0
    local direction = diff:Normalized()

    -- Calculate the angle difference
    local angle_difference = RotationDelta(VectorToAngles(unit.physicsVelocity:Normalized()), VectorToAngles(direction)).y
    
    -- Set the new velocity
    if math.abs(angle_difference) < 5 then
      -- CLAMP
      unit.physicsVelocity = spirit_speed * direction
    elseif angle_difference > 0 then
      unit.physicsVelocity = RotatePosition(Vector(0,0,0), QAngle(0,10,0), unit.physicsVelocity)
    else    
      unit.physicsVelocity = RotatePosition(Vector(0,0,0), QAngle(0,-10,0), unit.physicsVelocity)
    end

    current_position = unit:GetAbsOrigin() + unit.physicsVelocity
    unit:SetAbsOrigin(current_position)

    -- COLLISION CHECK
    local distance = (unit.targetPos - current_position):Length()
    local collision = distance < 50

    -- MAX DISTANCE CHECK
    local distance_to_caster = (source - current_position):Length()
    if distance > max_distance then 
      unit:SetAbsOrigin(source)
      unit.state = "acquiring" 
    end

    -- Acquiring finds a random point
    if unit.state == "acquiring" then
      unit.state = "target_acquired"
      unit.current_target = nil
      unit.idling = true
      unit.targetPos = source + RandomVector(RandomInt(radius/2, radius))
      unit.targetPos.z = GetGroundHeight(unit.targetPos,nil)
      
      -- print("Waiting for attack time. Acquiring -> Random Point Target acquired")
      if Debug then DebugDrawCircle(unit.targetPos, idleColor, 100, 25, true, draw_duration) end

    -- If the state was to follow a target enemy, it means the unit can perform an attack.    
    elseif unit.state == "target_acquired" then

      -- Update the point of the target's current position
      if unit.current_target then
        unit.targetPos = unit.current_target:GetAbsOrigin()
        if Debug then DebugDrawCircle(unit.targetPos, targetColor, 100, 25, true, draw_duration) end
      end

      -- Do physical damage here, and increase hit counter. 
      if collision then

        -- If the target was an enemy and not a point, the unit collided with it
        if unit.current_target ~= nil then
          
          -- Damage, units will still try to collide with attack immune targets but the damage wont be applied
          if not unit.current_target:IsAttackImmune() then
            local damage_table = {
              attacker = caster,
              damage_type = abilityDamageType,
              damage = damage,
              victim = unit.current_target,
            }

            ApplyDamage(damage_table)

            -- Damage particle
            local particle = ParticleManager:CreateParticle(particleDamage, PATTACH_ABSORIGIN, unit.current_target)
            ParticleManager:SetParticleControl(particle, 0, unit.current_target:GetAbsOrigin())
            ParticleManager:SetParticleControlEnt(particle, 1, unit.current_target, PATTACH_POINT_FOLLOW, "attach_hitloc", unit.current_target:GetAbsOrigin(), true)

            -- Fire Sound on the target unit
            unit.current_target:EmitSound("Hero_DeathProphet.Exorcism.Damage")
            
            -- Set to return
            unit.state = "acquiring"

            -- Trigger Caster AI to finish turn
            caster.ai:finishedAction()
          end

        -- In other case, its a point, reacquire target or return to the caster (50/50)
        else
          unit.state = "acquiring"
        end
      end
    end

    return 0.1
  end)
end

-- Kill all units when the owner dies or the spell is cast while the first one is still going
function ExorcismDeath( event )
  local caster = event.caster
  local targets = caster.spirits or {}

  caster:StopSound("Hero_DeathProphet.Exorcism")
  for _,unit in pairs(targets) do   
    if not unit:IsNull() then
      unit:ForceKill(false)
    end
  end
end
