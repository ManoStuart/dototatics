function Splash(keys)
  local caster = keys.caster
  local target = keys.target
  local ability = keys.ability
  local radius = ability:GetLevelSpecialValueFor("radius", 0)
  local abilityDamageType = ability:GetAbilityDamageType()
  local abilityTargetType = ability:GetAbilityTargetType()
  
  -- Finding the units for each radius
  local enemies = FindUnitsInRadius(caster:GetTeam(), target:GetAbsOrigin(), nil, radius, DOTA_UNIT_TARGET_TEAM_ENEMY, abilityTargetType, 0, 0, false)

  -- Initializing the damage table
  local damage_table = {}
  damage_table.attacker = caster
  damage_table.damage_type = abilityDamageType
  damage_table.damage = caster:GetAttackDamage()

  --loop for doing the splash damage while ignoring the original target
  for i,v in ipairs(enemies) do
    if v ~= target then 
      damage_table.victim = v
      ApplyDamage(damage_table)
    end
  end
end