function Start(keys)
  local target = keys.target
  local caster = keys.caster
  local ability = keys.ability
  local level = (ability:GetLevel() - 1)
  local duration = ability:GetLevelSpecialValueFor("duration", level)
  local heal = ability:GetLevelSpecialValueFor("heal", level)

  if caster:GetTeam() == target:GetTeam() then
    ability.rpg.allied = 1
    
    -- Apply the healing version of the Urn modifier.
    if GameRules.inCombat then
      ability:ApplyDataDrivenModifier(caster, target, "modifier_item_sacred_potion_heal", nil)

    -- Instantly heal outside combat
    else target:Heal(duration * heal, caster) end


  -- Apply the damaging version of the Urn modifier.
  else
    if not GameRules.inCombat then return end

    ability:ApplyDataDrivenModifier(caster, target, "modifier_item_sacred_potion_damage", nil)
    ability.rpg.enemy = 1
  end

  -- Decrement the charges on the Urn by one.
  ability:SetCurrentCharges(ability:GetCurrentCharges() - 1)
  target:EmitSound("DOTA_Item.UrnOfShadows.Activate")
end


function OnDeath(keys)
  for i=0, 5, 1 do
    local item = keys.caster:GetItemInSlot(i)

    if item ~= nil then
      if item:GetName() == "item_sacred_potion" then
        if (item.rpg and not keys.unit.isBoss) then return end

        if item.rpg then item.rpg.charges = 2
        else item.rpg = {charges = 1}
        end

        item:SetCurrentCharges(item:GetCurrentCharges() + 1)
        return
      end
    end
  end
end