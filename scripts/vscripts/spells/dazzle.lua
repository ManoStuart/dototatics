function ShadowWaveRitual(keys)
  local target = keys.target
  local ability = keys.ability
  local particleName = keys.shadow_wave_particle

  local radius = ability:GetLevelSpecialValueFor("bounce_radius", ability:GetLevel() - 1)
  local units = FindUnitsInRadius(target:GetTeam(), target:GetAbsOrigin(), nil, radius, DOTA_UNIT_TARGET_TEAM_FRIENDLY, ability:GetAbilityTargetType(), 0, 0, false)

  for _,v in pairs(units) do
    -- Create the particle for the visual effect
    local particle = ParticleManager:CreateParticle(particleName, PATTACH_CUSTOMORIGIN, target)
    ParticleManager:SetParticleControlEnt(particle, 0, target, PATTACH_POINT_FOLLOW, "attach_hitloc", target:GetAbsOrigin(), true)
    ParticleManager:SetParticleControlEnt(particle, 1, v, PATTACH_POINT_FOLLOW, "attach_hitloc", v:GetAbsOrigin(), true)

    target = v
  end
end

function ShallowGrave(keys)
  local ability = keys.ability
  local entity = keys.unit
  local min = ability:GetLevelSpecialValueFor("min_health", ability:GetLevel() - 1)

  -- Show effect when unit is in min health
  if entity:GetHealth() <= min then
    entity:SetHealth(min)
    local particle = ParticleManager:CreateParticle(keys.particle, PATTACH_ABSORIGIN_FOLLOW, entity)

    Timers:CreateTimer(2, function()
      ParticleManager:DestroyParticle(particle, true)
    end)

    entity:EmitSound(keys.sound)
  end
end