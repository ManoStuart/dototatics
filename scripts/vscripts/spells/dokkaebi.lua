function Steal(keys)
  local entity = keys.target
  local caster = keys.caster
  local items = {}

  local aux = caster.movType

  caster:RemoveAbility("dokkaebi_steal")

  Timers:CreateTimer(0.4, function()
    local ability = caster:AddAbility("dokkaebi_flee")
    ability:SetLevel(1)
    -- Change entity AI
    caster.ai = require("ais/escape_ai")
    caster.ai = caster.ai:start(caster)
  end)

  Timers:CreateTimer(1.5, function()
    caster.movType = aux
  end)

  -- equiped items
  for k,v in pairs(entity.equip) do
    if v.item ~= nil and k ~= 0 then
      items[#items+1] = {equiped = true, slot = k}
    end
  end

  -- inventory items
  for i = 0, 5 do
    if entity:GetItemInSlot(i) then
      items[#items+1] = {equiped = false, slot = i}
    end
  end

  -- Random a item to be stolen --- Cant steal weapon
  if #items == 0 then return end
  local rand = RandomInt(1, #items)

  local stolen
  if items[rand].equiped then
    stolen = GameRules.UnequipItem(entity, items[rand].slot, false).item
    GameRules.Equip(entity, nil, items[rand].slot, false)
  else
    stolen = entity:GetItemInSlot(items[rand].slot)
  end

  local item = CreateItem(stolen:GetName(), entity, entity)

  -- Store development
  item.ap = stolen.ap
  item.rpg = stolen.rpg

  -- Set consumables charges
  item:SetCurrentCharges(stolen:GetCurrentCharges())

  -- Swap items
  entity:RemoveItem(stolen)
  caster:AddItem(item)

  -- Send player a notification
  CustomGameEventManager:Send_ServerToPlayer(
    entity:GetPlayerOwner(),
    "banner_msg",
    {msg = " was stolen!", loc = item:GetName(), sub = "Kill Dokkaebi before he runs away!"}
  )
end