function Movement(keys)
  if GameRules.inCombat then return end
  
  GameRules.StartMovement(keys.caster, 5)
end

function Flee(keys)
  local caster = keys.caster
  local ability = keys.ability
  local level = ability:GetLevel() - 1

  local exp = ability:GetLevelSpecialValueFor("exp_reduct", level)
  local gold = ability:GetLevelSpecialValueFor("gold_reduct", level)
  local ap = ability:GetLevelSpecialValueFor("ap_reduct", level)

  -- Remove some of the encounter rewards
  CombatManager:ChangeRewards(exp, 0, gold, 0, ap, 0)

  -- Send player a notification
  CustomGameEventManager:Send_ServerToAllClients(
    "banner_msg",
    {msg = " fled from battle!", loc = caster:GetUnitName(), sub = "Too bad bro."}
  )

  -- Remove from combat
  CombatManager:UnitKilled(caster)
end

function SpawnUnit(keys)
  local caster = keys.caster
  local target = keys.target or keys.caster
  local pos = target:GetAbsOrigin()

  for i = 1, keys.count do
    local vec = RandomVector(keys.range)
    local entity = CreateUnitByName(keys.unitName, pos + vec, true, nil, nil, caster:GetTeamNumber())

    CombatManager:AddNewUnit(entity, 1)
  end

  -- Update HUD
  CombatManager:UpdateQueueHUD()
end

function ChangeTeam(keys)
  local casterTeam = keys.caster:GetTeamNumber()

  local team = DOTA_TEAM_GOODGUYS
  if keys.targetTeam == "enemy" then team = (casterTeam == DOTA_TEAM_GOODGUYS) and DOTA_TEAM_BADGUYS or DOTA_TEAM_GOODGUYS
  elseif keys.targetTeam == "friendly" then team = casterTeam
  elseif keys.targetTeam == "neutral" then team = DOTA_TEAM_NEUTRALS
  end

  CombatManager:ChangeTeams(keys.target, team)
end