function Start(keys)  
  local dagon_particle = ParticleManager:CreateParticle("particles/items_fx/dagon.vpcf",  PATTACH_ABSORIGIN_FOLLOW, keys.caster)
  ParticleManager:SetParticleControlEnt(dagon_particle, 1, keys.target, PATTACH_POINT_FOLLOW, "attach_hitloc", keys.target:GetAbsOrigin(), false)
  local particle_effect_intensity = 300 + (100 * keys.dagonLevel)  --Control Point 2 in Dagon's particle effect takes a number between 400 and 800, depending on its level.
  ParticleManager:SetParticleControl(dagon_particle, 2, Vector(particle_effect_intensity))
  
  keys.caster:EmitSound("DOTA_Item.Dagon.Activate")
  keys.target:EmitSound("DOTA_Item.Dagon5.Target")
end

function CheckMana(keys)
  local caster = keys.caster
  local ability = keys.ability
  local mana = keys.manaCost

  if caster:GetMana() > mana then
    caster:SpendMana(mana, nil)
  else
    caster:Stop()
    CustomGameEventManager:Send_ServerToPlayer(
      caster:GetPlayerOwner(),
      "banner_msg",
      {msg = "Not enough mana!"}
    )
  end
end