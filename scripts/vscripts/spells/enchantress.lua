function AddWisp(keys)
  local entity = keys.target
  local caster = keys.caster
  local ability = keys.ability
  local modifier = "modifier_wisp_vfx" .. RandomInt(1,4)

  ability:ApplyDataDrivenModifier(caster, entity, modifier, {})

  modifier = "modifier_wisp_count"
  local current = entity:GetModifierStackCount(modifier, ability)
  if current >= 1 then
    entity:SetModifierStackCount(modifier, ability, current + 1)
  else
    ability:ApplyDataDrivenModifier(caster, entity, modifier, {})
    entity:SetModifierStackCount(modifier, ability, 1)
  end
end

function RemoveWisp(keys)
  local entity = keys.caster
  local ability = keys.ability
  local rand = RandomInt(1,4)

  local modifier = "modifier_wisp_count"
  local current = entity:GetModifierStackCount(modifier, ability)
  if current <= 1 then
    entity:RemoveModifierByName(modifier)
  else
    entity:SetModifierStackCount(modifier, ability, current - 1)
  end

  local i = 0
  while (i < 5) do
    modifier = "modifier_wisp_vfx" .. rand

    if entity:FindModifierByName(modifier) then
      return entity:RemoveModifierByName(modifier)
    end

    rand = (rand % 4) + 1
    i = i + 1
  end
end

function PurgeWisps(keys)
  local caster = keys.caster
  local ability = keys.ability

  local modifier = "modifier_wisp"
  while (caster:FindModifierByName(modifier)) do
    caster:RemoveModifierByName(modifier)
  end
end

function WispCast(keys)
  local entity = keys.target
  local caster = keys.caster
  local ability = keys.ability
  local level = (ability:GetLevel() - 1)
  local heal = ability:GetLevelSpecialValueFor("heal", level)
  local cooldown = ability:GetCooldown(level)

  if not caster:FindModifierByName("modifier_wisp") and GameRules.inCombat then
    Timers:CreateTimer(0.2, function()
      CombatManager:UndoFailedAction(caster, cooldown)
    end)
    return
  end

  caster:RemoveModifierByName("modifier_wisp")

  entity:Heal(heal, caster)
  ParticleManager:CreateParticle("particles/units/heroes/hero_enchantress/enchantress_natures_attendants_heal.vpcf", PATTACH_ABSORIGIN_FOLLOW, entity)
end

function Juno(keys)
  print("juno started")
  GameRules.encounters.desert3.vec[5] = nil
end