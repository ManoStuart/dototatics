List = {}
function List.new ()
  return nil
end


function List.newNode(key, value)
  return {
    next = nil,
    key = key,
    value = value,
  }
end


-- Call it with either a created List node on value and nil on key, or actual key and values
function List.insert (list, value, key)
  -- Parse arguments
  local new = nil
  if key == nil then
    key = value.key
    new = value
  else
    new = List.newNode(key, value)
  end

  -- Find node position
  local aux = nil
  local tmp = list
  while tmp ~= nil and tmp.key <= key do
    if (tmp.key == key) then key = key + 1 end
    aux = tmp
    tmp = tmp.next
  end

  -- reset key in case it was a duplicate
  new.key = key
  new.value.queueKey = key

  -- Place node
  new.next = tmp
  if (aux ~= nil) then
    aux.next = new
    return list
  else
    return new
  end
end

function List.pop (list)
  local aux = list.next
  list = nil

  return aux
end

function List.remove(list, key)
  local aux = nil
  local tmp = list
  while tmp ~= nil and tmp.key ~= key do
    aux = tmp
    tmp = tmp.next
  end

  if tmp == nil then return list end

  if aux ~= nil then
    -- Remove node from it's current place
    aux.next = tmp.next
  else
    -- First node removed
    list = tmp.next
  end

  return list
end

function List.changeKey (list, old, new)
  local aux = nil
  local tmp = list
  while tmp ~= nil and tmp.key ~= old do
    aux = tmp
    tmp = tmp.next
  end

  if tmp == nil then return list end

  if aux ~= nil then
    -- Remove node from it's current place
    aux.next = tmp.next
  else
    -- First node removed
    list = tmp.next
  end

  -- Change node key
  tmp.key = new
  tmp.value.queueKey = new

  -- Reinsert
  return List.insert(list, tmp)
end

function List.findEntity(list, id)
  local tmp = list
  while tmp ~= nil and tmp.value:entindex() ~= id do
    tmp = tmp.next
  end

  return tmp
end

function List.toNettable(list)
  local tmp = list
  local aux = {}

  while tmp ~= nil do
    aux[#aux+1] = {
      player = tmp.value:GetMainControllingPlayer(),
      entity = tmp.value:entindex(),
      portrait = tmp.value:GetUnitName(),
      pos = tmp.key,
    }
    
    tmp = tmp.next
  end

  return aux
end

function List.GetEntities(list)
  local tmp = list
  local aux = {}

  while tmp ~= nil do
    aux[#aux+1] = tmp.value
    tmp = tmp.next
  end

  return aux
end