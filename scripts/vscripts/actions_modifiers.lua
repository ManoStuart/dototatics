--------------------------------------------------------------------------------
-- UTILS
--------------------------------------------------------------------------------
function FinishAction(entity, move, ap)
  if entity:GetPlayerOwnerID() ~= -1 then
    CustomGameEventManager:Send_ServerToPlayer(
      entity:GetPlayerOwner(),
      "finished_action",
      {hero = entity:entindex(), action = move}
    )
  end
  
  GameRules.CombatManager:FinishAction(entity, move, ap)

  entity:Stop()
  entity:Hold()
end

function AddRestrictions(entity)
  GameRules.Overseer:CastTarget(entity, "silence")

  if not entity.atkType then entity.atkType = entity:GetAttackCapability() end

  if entity:GetPlayerOwnerID() ~= -1 then
    entity:SetAttackCapability(DOTA_UNIT_CAP_NO_ATTACK)
    entity:SetMoveCapability(DOTA_UNIT_CAP_MOVE_NONE)
  end

  -- Enable combat abilities
  -- 0: Combat only, 1: Field only 2: both, -1: field only but start hidden
  for i = 0, entity:GetAbilityCount() do
    local ability = entity:GetAbilityByIndex(i)
    if not ability then break end

    local type = ability:GetLevelSpecialValueFor("encounter_only", 0)
    if type == 0 or type == 2 then
      ability:SetHidden(false)
    else
      ability:SetHidden(true)
    end
  end

  entity:Hold()
end

function StopUnit(entity)
  entity:RemoveModifierByName(GameRules.Overseer.modifiers.action)
  entity:RemoveModifierByName(GameRules.Overseer.modifiers.move)
end

function StartUnit(entity)
  entity:Hold()

  GameRules.Overseer:CastTarget(entity, "action")
  GameRules.Overseer:CastTarget(entity, "move")
end

function RemoveRestrictions(entity)
  if entity:IsNull() then return end

  entity:RemoveModifierByName(GameRules.Overseer.modifiers.action)
  entity:RemoveModifierByName(GameRules.Overseer.modifiers.move)

  Timers:CreateTimer(0.2, function()
    if entity:IsNull() then return end
    
    entity:RemoveModifierByName("modifier_cannot_cast_datadriven")
    entity:SetAttackCapability(entity.atkType)
    entity:SetMoveCapability(entity.movType)

    -- Enable combat abilities
    for i = 0, entity:GetAbilityCount() do
      local ability = entity:GetAbilityByIndex(i)
      if not ability then break end

      if ability:GetLevelSpecialValueFor("encounter_only", 0) >= 1 then
        ability:SetHidden(false)
      else
        ability:SetHidden(true)
      end
    end
  end)
end

--------------------------------------------------------------------------------
-- ACTION CALLBACKS
--------------------------------------------------------------------------------
function ActionInit(keys)
  keys.target:RemoveModifierByName("modifier_cannot_cast_datadriven")
  keys.target:SetAttackCapability(keys.target.atkType)
end

function OnAttack(keys)
  keys.attacker:RemoveModifierByName("modifier_has_action_datadriven")
  FinishAction(keys.attacker, "attack", keys.attacker.atkSpeed)

  if keys.attacker:GetPlayerOwnerID() == -1 then
    keys.attacker.ai:finishedAction()
  end
end

function OnCast(keys)
  keys.unit:RemoveModifierByName("modifier_has_action_datadriven")

  local ability = keys.event_ability
  FinishAction(keys.unit, "cast", ability:GetCooldown(ability:GetLevel()-1))

  Timers:CreateTimer(0.5, function()
      if ability:IsNull() then return end
      ability:EndCooldown()
    end
  )

  if keys.unit:GetPlayerOwnerID() == -1 then
    keys.unit.ai:finishedAction()
  end
end

function ActionDestroy(keys)
  if keys.target:IsNull() then return end

  keys.target:SetAttackCapability(DOTA_UNIT_CAP_NO_ATTACK)
  GameRules.Overseer:CastTarget(keys.target, "silence")
end

function OnDeath(keys)
  if GameRules.inInteraction then return end

  GameRules.CombatManager:UnitKilled(keys.unit)

  GameRules:checkDrop(keys.unit)

  keys.target = keys.unit
  ActionDestroy(keys)
end

--------------------------------------------------------------------------------
-- MOVE CALLBACKS
--------------------------------------------------------------------------------
function MoveInit(keys)
  local target = keys.target

  target:SetMoveCapability(target.movType)
  target.position = target:GetAbsOrigin()
  target.remainingMovement = target:GetMoveSpeedModifier(target:GetBaseMoveSpeed())
end

function OnMove(keys)
  local target = keys.target

  local old_position = target.position
  local new_position = target:GetAbsOrigin()
  local distance = (new_position - old_position):Length2D()

  target.remainingMovement = target.remainingMovement - distance

  -- Checks if unit can has any movement left
  if target.remainingMovement <= 0 then
    target:RemoveModifierByName("modifier_can_move_datadriven")
    FinishAction(target, "move", 0)

    if target:GetPlayerOwnerID() == -1 then
      target.ai:finishedMove()
    end
  end

  -- Saves the new position for the next check
  target.position = new_position
end

function MoveDestroy(keys)
  keys.target:SetMoveCapability(DOTA_UNIT_CAP_MOVE_NONE)
end

function OnMoveDeath(keys)
  keys.target = keys.unit
  MoveDestroy(keys)
end


--------------------------------------------------------------------------------
-- INTERACTION CALLBACKS
--------------------------------------------------------------------------------
function HeroSetup(keys)
  local caster = keys.caster
  caster.atkType = DOTA_UNIT_CAP_NO_ATTACK
  caster.movType = DOTA_UNIT_CAP_MOVE_GROUND
  caster.atkSpeed = 0
  caster.equip = {}
  caster.rpg = {}
  caster.haste = 0
  caster.encounterRate = 0
  caster.curSound = {mute = true, count = 0}

  SoundManager:StartSound(caster, "DotaTatics.MainCity", true, true)

  Timers:CreateTimer(0.5, function ()
    keys.ability:UpgradeAbility(true)
    caster:SetAbilityPoints(0)
  end)
end


function OnOutsideDeath(keys)
  if GameRules.inCombat then return end
  local entity = keys.caster
  local pos = Entities:FindByClassname(nil, "info_player_start_goodguys")

  -- Revive plebs
  Timers:CreateTimer(1.5, function()
    if entity:IsNull() then return end
    entity:RespawnHero(false, false, false)
  end)

  -- Reset dead faggot to main city
  Timers:CreateTimer(1.7, function()
    if entity:IsNull() then return end
    FindClearSpaceForUnit(entity, pos:GetOrigin(), true)
  end)

  GameRules:checkDrop(entity)
end