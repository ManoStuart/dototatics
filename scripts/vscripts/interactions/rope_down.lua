local obj = {}
obj.__index = obj

function obj:start(entity, unit)
  if entity:GetOrigin().z > 680 or entity:GetOrigin().z < 620 then
    obj:cancel(entity)
    return print("cheating on rope")
  end

    GameRules.SwapControls(entity, false, false)
  GameRules.Overseer:CastTarget(entity, "walkAnimation")

  local pos = Entities:FindByName(nil, "rope1")
  local vec = (pos:GetOrigin() - entity:GetOrigin())
  local vel = vec:Length() / 4
  vec = vec:Normalized()

  local pos2 = Entities:FindByName(nil, "rope2")
  local vec2 = (pos2:GetOrigin() - pos:GetOrigin())
  local vel2 = vec2:Length() / 6
  vec2 = vec2:Normalized()

  local dur = 0
  Timers:CreateTimer(0.1, function()
    -- Up n down logic
    if (dur < 0.4) then entity:SetOrigin(entity:GetOrigin() + (vec * vel))
    else entity:SetOrigin(entity:GetOrigin() + (vec2 * vel2))
    end

    -- Check if we finished jumping
    dur = dur + 0.1
    if dur >= 1 then
      entity.curInteraction = nil
      GameRules.SwapControls(entity, true, true)
      GameRules.Overseer:CastTarget(entity, "purgeAnimation")

      return CombatManager:ReenableAll()
    end
    return 0.1
  end)
end

function obj:finish(args, entity)
end

function obj:cancel(entity)
end

return obj