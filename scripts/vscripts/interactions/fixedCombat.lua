local obj = {first = true}
obj.__index = obj

function obj:start(entity, unit)

  CustomGameEventManager:Send_ServerToPlayer(
    entity:GetPlayerOwner(),
    "start_speech",
    {type = unit:GetName(), player = entity:GetPlayerOwnerID()}
  )

  return setmetatable({entity = entity, unit = unit}, self)
end

function obj:finish(args)
  if obj.first then
    GameRules.CombatManager.NextTurn()
    GameRules.inInteraction = false
    obj.first = false
  else
    obj.first = true
    self.entity.curInteraction = nil
  end
end

function obj:cancel()
  if obj.first then
    GameRules.inInteraction = false
    GameRules.CombatManager.NextTurn()
    obj.first = false
  else
    obj.first = true
    self.entity.curInteraction = nil
  end
end

function obj:finishCombat(won)
  if won then
    local name = self.unit:GetName()
    CustomGameEventManager:Send_ServerToPlayer(
      self.entity:GetPlayerOwner(),
      "start_speech",
      {type = name .. "Won", player = self.entity:GetPlayerOwnerID()}
    )

    -- Make map layer changes
    DoEntFire(name .. "_hide", "HideWorldLayerAndDestroyEntities", "", 0, nil, nil)
    DoEntFire(name .. "_show", "ShowWorldLayerAndSpawnEntities", "", 0, nil, nil)
  else
    -- Reset Slain boolean
    self.unit:Attribute_SetIntValue("slain", 0)
    obj.first = true
    self.entity.curInteraction = nil
  end
end

return obj