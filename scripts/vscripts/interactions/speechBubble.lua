local bubble = {}
local numBubble = 0
local bubbleId = 0

function Speak(keys)
  local ability = keys.ability
  local caster = keys.caster
  local name = caster:GetName()
  if name == "npc_dota_creature" then name = caster:GetUnitName() end

  caster:RemoveModifierByName("modifier_not_discovered")

  if bubble[name] or numBubble >= 4 then return end

  local msg = nil
  local obj = GameRules.speeches[name];
  if not obj then return end

  -- Get appropriate speech
  for _,v in pairs(obj) do
    if not v.cond or v.cond(keys.target) then
      msg = v.msg
      break
    end
  end

  -- Random what msg to use
  if type(msg) == "table" then msg = msg[RandomInt(1, #msg)] end

  -- Display speech bubble
  if not msg then return end
  caster:AddSpeechBubble(bubbleId, msg, 7, 0, 0)
  bubbleId = (bubbleId + 1) % 4

  -- Make we dont spam speeches
  numBubble = numBubble + 1
  bubble[name] = true
  local cd = ability:GetCooldown(ability:GetLevel()-1)
  Timers:CreateTimer(cd , function() bubble[name] = nil end)
  Timers:CreateTimer(7 , function() numBubble = numBubble - 1 end)
end