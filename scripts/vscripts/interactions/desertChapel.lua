local obj = {}
obj.__index = obj

function obj:start(entity, unit)
  GameRules.SwapControls(entity, false, true)

  local speech = "desertChapel"

  if unit:Attribute_GetIntValue("slain", 0) > 0 then
    if entity.rpg.omniknight and entity.rpg.omniknight > 0 then
      speech = "desertChapel_won"
    else
      speech = "desertChapel_extras"
    end
  elseif entity.rpg.omniknight and entity.rpg.omniknight > 0 then
    speech = "desertChapel_quest"
  end

  CustomGameEventManager:Send_ServerToAllClients(
    "start_speech",
    {type = speech, player = entity:GetPlayerOwnerID()}
  )

  return setmetatable({entity = entity, unit = unit}, self)
end

function obj:finish(args)
  -- Start Boss Battle
  if args.num == 1 then
    GameRules.inInteraction = false
    return GameRules.CombatManager:CreateCombat(self.entity, "desertChapel", true)

  -- Pray and Healing
  elseif args.num == 2 and self.entity.rpg.omniPrays < 3 then
    GameRules.Recover(self.entity, 9999, 0, 0)
    self.entity.rpg.omniPrays = self.entity.rpg.omniPrays + 1
  end

  CombatManager:ReenableAll()

  GameRules.SwapControls(self.entity, true, false)
  self.entity.curInteraction = nil
end

function obj:cancel()
  CombatManager:ReenableAll()

  GameRules.SwapControls(self.entity, true, false)
  self.entity.curInteraction = nil
  CustomGameEventManager:Send_ServerToAllClients("clear_interaction", {player = self.entity:GetPlayerOwnerID()})
end

function obj:finishCombat(won)
  if won then
    self.unit:Attribute_SetIntValue("slain", 1)
    self.entity.rpg.omniknight = self.entity.rpg.omniknight + 1
    self.entity.rpg.omniPrays = 0
  end

  self.entity.curInteraction = nil
end

return obj