local obj = {}
obj.__index = obj

function obj:start(entity, unit)
  GameRules.SwapControls(entity, false, true)

  local speech = "mainPriest"

  if entity:GetUnitName() == "npc_dota_hero_omniknight" then
    speech = "mainPriest_omniknight"

  elseif entity.rpg.omniknight == -1 or entity:GetUnitName() ~= "npc_dota_hero_jakiro" then
    speech = "mainPriest_extras"
  elseif unit:Attribute_GetIntValue("used", 0) > 0 then
    if entity.rpg.omniknight then
      if entity.rpg.omniknight == 1 then
        speech = "mainPriest_quest_ini"
      elseif entity.rpg.omniknight > 1 then
        speech = "mainPriest_quest"
      end
    else
      speech = "mainPriest_extras"
    end
  end

  CustomGameEventManager:Send_ServerToAllClients(
    "start_speech",
    {type = speech, player = entity:GetPlayerOwnerID()}
  )

  return setmetatable({entity = entity, unit = unit}, self)
end

function obj:finish(args)
  -- Start Boss Battle
  if args.num == 1 then
    self.entity.rpg.omniknight = 1
    self.unit:Attribute_SetIntValue("used", 1)

  -- Stay at the inn
  elseif args.num == 2 and self.entity:GetGold() > 40 then
    GameRules.Recover(self.entity, 9999, 9999, -40)

  -- Stay at the inn being a follower
  elseif args.num == 3 then
    GameRules.Recover(self.entity, 9999, 9999, 0)
  
  -- Never start quest
  elseif args.num == 4 then
    self.entity.rpg.omniknight = -1
  end


  CombatManager:ReenableAll()

  GameRules.SwapControls(self.entity, true, false)
  self.entity.curInteraction = nil
end

function obj:cancel()
  CombatManager:ReenableAll()

  GameRules.SwapControls(self.entity, true, false)
  self.entity.curInteraction = nil
  CustomGameEventManager:Send_ServerToAllClients("clear_interaction", {player = self.entity:GetPlayerOwnerID()})
end

return obj