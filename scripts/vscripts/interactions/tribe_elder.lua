local obj = {}
obj.__index = obj

function obj:start(entity, unit)
  GameRules.SwapControls(entity, false, true)

  local speech
  local aux = 0

  if entity:GetUnitName() == "npc_dota_hero_dazzle" then
    speech = "tribe_elder_dazzle"

  elseif entity:GetUnitName() ~= "npc_dota_hero_jakiro" then
    speech = "tribe_elder_other_hero"

  else
    local drums = false
    local urn = false

    for i=0, 5, 1 do
      local item = entity:GetItemInSlot(i)

      if item ~= nil then
        if item:GetName() == "item_drums" and aux <= 3 then
          drums = true
          speech = "tribe_elder_drums_urn"
          aux = 3
        end

        if item:GetName() == "item_sacred_potion" and aux <= 1 then
          aux = 1
          -- Used urn on both enemies and allies
          if item.rpg and item.rpg.enemy and item.rpg.allied then
            speech = "tribe_elder_urn_finish"

          -- Has urn but didnt use it fully yet
          else
            speech = "tribe_elder_urn_middle"
          end
        end

        if item:GetName() == "item_sacred_potion" and item:GetCurrentCharges() >= 1 then
          urn = true
        end
      end
    end

    if drums and urn then speech = "tribe_elder_drums_finish" end
  end

  if not speech then
    if entity.rpg.dazzle == 2 then
      speech = "tribe_elder_drums_middle"
    else
      speech = "tribe_elder"
    end
  end

  CustomGameEventManager:Send_ServerToAllClients(
    "start_speech",
    {type = speech, player = entity:GetPlayerOwnerID()}
  )

  return setmetatable({entity = entity, unit = unit}, self)
end

function obj:finish(args)
  local entity = self.entity

  -- Dazzle game iteration
  if entity.dazzleGame then
    GameRules.Overseer:CastTarget(entity, "dazzle")

    ApplyDamage({
      victim = entity,
      attacker = entity,
      damage = args.num * 30,
      damage_type = DAMAGE_TYPE_PURE
    })

    if entity:IsAlive() then
      entity.dazzleGame = entity.dazzleGame + 1

      -- Turn into dazzle
      if entity.dazzleGame > 5 then
        FinishRitual(entity)
        entity.rpg.dazzle = 3
        entity:AddItem(CreateItem("item_poison_stone_datadriven", entity, entity))
        GameRules.ChangeHero(entity, "npc_dota_hero_dazzle")

      -- Restart minigame
      else
        return Timers:CreateTimer(1.5, function()
          CustomGameEventManager:Send_ServerToPlayer(
            entity:GetPlayerOwner(),
            "start_timing",
            {player = entity:GetPlayerOwnerID(), num = 6 + entity.dazzleGame}
          )
        end)
      end
    else
      FinishRitual(entity)
    end

  else
    -- Start dazzle quest
    if args.num == 1 and not entity.rpg.dazzle then
      entity:AddItem(CreateItem("item_recipe_sacred_potion", entity, entity))
      entity.rpg.dazzle = 1

    -- Pay for Healing
    elseif args.num == 2 and entity:GetGold() > 50 then
      GameRules.Recover(entity, 0, 27, -50)

    -- Give Drums recipe
    elseif args.num == 3 and entity.rpg.dazzle == 1 then
      entity:AddItem(CreateItem("item_recipe_drums", entity, entity))
      entity:AddItem(CreateItem("item_staff_dazzle_datadriven", entity, entity))
      entity.rpg.dazzle = 2

    -- Start dazzle minigame
    elseif args.num == 4 and entity.rpg.dazzle == 2 then
      CustomGameEventManager:Send_ServerToAllClients("start_fade", {encounter = false})
      Timers:CreateTimer(0.7, function()
        StartRitual(entity)
      end)
      return
    end
  end

  CombatManager:ReenableAll()

  GameRules.SwapControls(entity, true, false)
  entity.curInteraction = nil
end

function obj:cancel()
  CombatManager:ReenableAll()

  GameRules.SwapControls(self.entity, true, false)
  self.entity.curInteraction = nil
  CustomGameEventManager:Send_ServerToAllClients("clear_interaction", {player = self.entity:GetPlayerOwnerID()})
end

function obj:finishCombat(won)
end

function StartRitual(entity)
  GameRules.duringEvent = true
  GameRules.SwapControls(entity, false, false)
  entity.dazzleGame = 1

  local pos = Entities:FindByName(nil, "tribe_point_10"):GetAbsOrigin()

  for i=0, 5, 1 do
    local item = entity:GetItemInSlot(i)

    if item ~= nil and item:GetName() == "item_sacred_potion" then
      entity:CastAbilityOnTarget(entity, item, entity:GetPlayerOwnerID())
    end
  end

  -- Reposition hero
  FindClearSpaceForUnit(entity, pos, true)

  local units = FindUnitsInRadius(entity:GetTeam(), pos, nil, 1000, DOTA_UNIT_TARGET_TEAM_FRIENDLY, DOTA_UNIT_TARGET_BASIC, 0, 0, false)

  -- Reposition tribe members and make them walk in circles
  for k,v in pairs(units) do
    GameRules.StopMovement(v)

    local rot = QAngle (0, 15 + 90 * k, 0)
    local dir = RotatePosition(Vector(0,0,0), rot, Vector(1,0,0))

    FindClearSpaceForUnit(v, pos + dir * 400, true)

    local order = {
      OrderType = DOTA_UNIT_ORDER_MOVE_TO_POSITION,
      UnitIndex = v:entindex(),
    }
    local i = k * 3

    -- Circles logic
    Timers:CreateTimer(0.7, function()
      if not GameRules.duringEvent then return end

      i = i + 1
      local rot = QAngle (0, 15 + 30 * i, 0)
      local dir = RotatePosition(Vector(0,0,0), rot, Vector(1,0,0))

      order.Position = pos + dir * 400

      ExecuteOrderFromTable( order )
      return 1
    end)
  end

  -- Create tribe ghosts and make them walk in circles
  Timers:CreateTimer(4.5, function()
    if not GameRules.duringEvent then return end

    local i = RandomInt(1, 12)
    local rot = QAngle (0, 15 + 30 * i, 0)
    local dir = RotatePosition(Vector(0,0,0), rot, Vector(1,0,0))

    -- Create ghosts
    local unit = CreateUnitByName(
      (RandomInt(0,1) == 1 and "npc_dota_peasant_tribe") or "npc_dota_peasant_tribe_elder",
      pos + dir * 250,
      true,
      nil,
      nil,
      DOTA_TEAM_GOODGUYS
    )

    -- Give ghost magic
    unit:AddAbility("ability_translucent"):SetLevel(1)
    unit:AddNewModifier(unit, nil, "modifier_invisible", {})

    local order = {
      queue = true,
      OrderType = DOTA_UNIT_ORDER_MOVE_TO_POSITION,
      UnitIndex = unit:entindex(),
    }

    -- Circles logic
    local n = 0
    local clock = RandomInt(0,1) == 1 and 1 or -1
    Timers:CreateTimer(0.7, function()
      if (n > 20) then return unit:Destroy() end

      n = n + 1
      i = i + clock
      local rot = QAngle (0, 15 + 30 * i, 0)
      local dir = RotatePosition(Vector(0,0,0), rot, Vector(1,0,0))

      order.Position = pos + dir * 250

      ExecuteOrderFromTable( order )
      return 0.5
    end)

    return RandomInt(3,7)
  end)

  CustomGameEventManager:Send_ServerToPlayer(
    entity:GetPlayerOwner(),
    "start_timing",
    {player = entity:GetPlayerOwnerID(), num = 7}
  )
end

function FinishRitual(entity)
  entity.dazzleGame = nil
  GameRules.duringEvent = false

  local pos = Entities:FindByName(nil, "tribe_point_10"):GetAbsOrigin()
  local units = FindUnitsInRadius(entity:GetTeam(), pos, nil, 1000, DOTA_UNIT_TARGET_TEAM_FRIENDLY, DOTA_UNIT_TARGET_BASIC, 0, 0, false)

  -- Restart npc movements
  for k,v in pairs(units) do
    GameRules.StartMovement(v, 1)
  end
end

return obj