local obj = {}
obj.__index = obj

function obj:start(entity, unit)
  if entity:GetOrigin().z > 680 or entity:GetOrigin().z < 620 then
    obj:cancel(entity)
    return print("cheating on timming game")
  end

  CustomGameEventManager:Send_ServerToPlayer(
    entity:GetPlayerOwner(),
    "start_timing",
    {player = entity:GetPlayerOwnerID(), num = 5}
  )
  GameRules.SwapControls(entity, false, true)

  return setmetatable({entity = entity, unit = unit}, self)
end

function obj:finish(args)
  local entity = self.entity
  local pos
  local dur = 0
  local switch = 0.5
  local zVel = -25

  -- Check jump success
  if args.num < 0.3 then
    pos = Entities:FindByName(nil, "jump_success")
  else
    pos = Entities:FindByName(nil, "jump_fail")
    switch = 0.3
  end

  -- Initializing jump variables
  local vec = pos:GetOrigin() - entity:GetOrigin()
  local vel = vec:Length2D() / 10

  if switch == 0.3 then
    vec.z = vec.z - 75
    zVel = vec.z / 7
  end

  vec.z = 0
  vec = vec:Normalized()

  GameRules.Overseer:CastTarget(entity, "walkAnimation")
  GameRules.SwapControls(entity, false, false)
  Timers:CreateTimer(0.1, function()
    local new = entity:GetOrigin() + (vec * vel)

    -- Up n down logic
    if (dur < switch) then new.z = new.z + 25
    else new.z = new.z + zVel
    end

    entity:SetOrigin(new)

    -- Check if we finished jumping
    dur = dur + 0.1
    if dur >= 1 then
      entity.curInteraction = nil
      GameRules.SwapControls(entity, true, false)
      GameRules.Overseer:CastTarget(entity, "purgeAnimation")

      return CombatManager:ReenableAll()
    end
    return 0.1
  end)
end

function obj:cancel()
  CombatManager:ReenableAll()
  
  GameRules.SwapControls(self.entity, true, false)
  self.entity.curInteraction = nil

  CustomGameEventManager:Send_ServerToPlayer(
    self.entity:GetPlayerOwner(),
    "clear_interaction",
    {player = self.entity:GetPlayerOwnerID()}
  )
end

return obj