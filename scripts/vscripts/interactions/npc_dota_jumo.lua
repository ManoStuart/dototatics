local obj = {}
obj.__index = obj

function obj:start(entity, unit)
  GameRules.SwapControls(entity, false, true)

  local speech = "jumo"
  if unit.enchantress then speech = "jumo_extra" end

  CustomGameEventManager:Send_ServerToAllClients(
    "start_speech",
    {type = speech, player = entity:GetPlayerOwnerID()}
  )

  return setmetatable({entity = entity, unit = unit}, self)
end

function obj:finish(args)
  CombatManager:ReenableAll()

  GameRules.SwapControls(self.entity, true, false)
  self.entity.curInteraction = nil
end

function obj:cancel()
  CombatManager:ReenableAll()

  GameRules.SwapControls(self.entity, true, false)
  self.entity.curInteraction = nil
  CustomGameEventManager:Send_ServerToAllClients("clear_interaction", {player = self.entity:GetPlayerOwnerID()})
end

return obj