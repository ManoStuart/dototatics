function GameRules.Recover(entity, mana, health, gold)
  GameRules.Overseer:CastTarget(entity, "dummy")

  if mana > 0 then
    entity:GiveMana(mana)
  elseif mana < 0 then
    entity:ReduceMana(-mana)
  end

  if health > 0 then
    entity:Heal(health, nil)
  elseif health < 0 then
    entity:ModifyHealth(entity:GetHealth() + health, nil, false, 0)
  end

  if gold > 0 then
    entity:ModifyGold(gold, true, DOTA_ModifyGold_AbilityCost)
  elseif gold < 0 then
    entity:SpendGold(-gold, DOTA_ModifyGold_AbilityCost)
  end
end

function GameRules.SwapControls(entity, a, b)
  entity:SwapAbilities("dota_ability_interact", "dota_ability_cancel_interaction", a, b)
end

function GameRules.UnequipItem(entity, slot, drop)
  if not entity.equip[slot] then return end

  if not entity.equip[slot].modifier or not entity.equip[slot].item then
    -- Trying to unequip fake weapon
    if slot == 1 then
      GameRules.UnequipItem(entity, 0, drop)
      GameRules.Equip(entity, nil, 0, false)
    end

    entity.equip[slot] = nil
    return
  end

  local modifier = entity.equip[slot].modifier

  if entity.equip[slot].stackable then
    -- Get the current stack count
    local current_stack = entity:GetModifierStackCount(modifier, entity)

    -- If its 1 then remove the modifier entirely, otherwise just reduce the stack number by 1
    if current_stack <= 1 then
      entity:RemoveModifierByName(modifier)
    else
      entity:SetModifierStackCount(modifier, entity, current_stack - 1)
    end
  else
    entity:RemoveModifierByName(modifier)
  end

  local obj = {item = CreateItem(entity.equip[slot].item, entity, entity)}

  -- Remove Ability
  if entity.equip[slot].ability then
    entity:RemoveAbility(entity.equip[slot].ability)
    
    -- Store development
    obj.item.ap = entity.equip[slot].ap
    obj.item.rpg = entity.equip[slot].rpg
  end

  -- Remove AtkSpeed
  entity.atkSpeed = entity.atkSpeed - entity.equip[slot].atkSpeed

  -- Remove item cooldown usage
  obj.item:EndCooldown()

  if drop then obj.phys = CreateItemOnPositionForLaunch(entity:GetOrigin(), obj.item)
  else entity:AddItem(obj.item) end

  entity.equip[slot] = nil

  -- Reequip offhand
  if slot == 0 and entity.equip[1] and entity.equip[1].weapon then
    entity.equip[0] = entity.equip[1]

    -- Recalculate TP usage
    entity.atkSpeed = entity.atkSpeed + entity.equip[0].atkSpeed
    entity.equip[0].atkSpeed = entity.equip[0].atkSpeed * 2
    entity.equip[1] = nil

    -- Update HUD
    GameRules.Equip(entity, nil, 1, false)
    GameRules.Equip(entity, entity.equip[0].item, 0, false)
  else
    GameRules.UpdateTP(entity)
  end

  return obj
end

function GameRules.UpdateTP(entity)
  CustomGameEventManager:Send_ServerToPlayer(
    entity:GetPlayerOwner(),
    "tp_update",
    {tp = entity.atkSpeed, haste = entity.haste}
  )
end

function GameRules.Equip(entity, item, pos, fake)
  CustomGameEventManager:Send_ServerToPlayer(
    entity:GetPlayerOwner(),
    "equip",
    {pos = pos, item = item, fake = fake, tp = entity.atkSpeed}
  )
end

function GameRules.ChangeHero(entity, hero)
  if entity:GetUnitName() ~= "npc_dota_hero_jakiro" then return end

  local equips = entity.equip
  local items = {}
  local rpg = entity.rpg
  local sounds = entity.curSound

  -- Get backpack and stash items
  for i=0, 11, 1 do
    local item = entity:GetItemInSlot(i)

    if item ~= nil then
      local newItem = CreateItem(item:GetName(), nil, nil)
      items[#items+1] = newItem

      -- Store development
      newItem.ap = item.ap
      newItem.rpg = item.rpg

      -- Set consumables charges
      newItem:SetCurrentCharges(item:GetCurrentCharges())
    end
  end

  -- Change current hero
  local newHero = PlayerResource:ReplaceHeroWith(
    entity:GetPlayerOwnerID(),
    hero,
    entity:GetGold(),
    entity:GetCurrentXP()
  )

  -- Restart custom fields
  newHero.atkType = DOTA_UNIT_CAP_NO_ATTACK
  newHero.atkSpeed = 0
  newHero.equip = {}
  newHero.haste = 0
  newHero.rpg = rpg
  newHero.encounterRate = 0
  newHero.curSound = sounds

  SoundManager:StartSound(newHero, "DotaTatics.FinishQuest", false, false)

  Timers:CreateTimer(0.5, function ()
    -- Get movement capability
    if not newHero:HasMovementCapability() then
      newHero.movType = DOTA_UNIT_CAP_MOVE_NONE
    elseif newHero:HasFlyMovementCapability() then
      newHero.movType = DOTA_UNIT_CAP_MOVE_FLY
    else
      newHero.movType = DOTA_UNIT_CAP_MOVE_GROUND
    end

    newHero:FindAbilityByName("dota_ability_interact"):UpgradeAbility(true)
    newHero:SetAbilityPoints(0)

    -- Reequip items 
    for k,v in pairs(equips) do
      if v.item then 
        local item = CreateItem(v.item, nil, nil)
        newHero:AddItem(item)
        item:CastAbility()

        newHero.equip[k].ap = v.ap
        newHero.equip[k].rpg = v.rpg

        -- Recalculate abilities level
        if newHero.equip[k].ability then
          local newAbility = entity:FindAbilityByName(newHero.equip[k].ability)

          newAbility:SetLevel(newHero.equip[k].ap / 100)
        end
      end
    end

    -- Give backpack items
    for k,v in pairs(items) do
      newHero:AddItem(v)
    end

    entity:Destroy()
  end)
end