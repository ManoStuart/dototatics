require ("timers")

local ai = {}
ai.__index = ai

function ai:start(entity)
  local abilities = {}

  local aux = GameRules.unitKV[entity:GetUnitName()]
  if not aux then return print("shits fucked up with unitKV") end

  local level = entity:GetLevel()

  entity.atkSpeed = type(aux.AtkSpeed) == "string" and aux.AtkSpeed:split(" ")[level] or aux.atkSpeed
  entity.movType = GameRules.movTypes[aux.MovementCapabilities]
  entity.haste = 0

  Timers:CreateTimer(0.5, function()
    if entity:IsNull() then return end
    
    for i = 0, entity:GetAbilityCount() do
      local ability = entity:GetAbilityByIndex(i)
      if not ability then break end

      local level = (ability:GetLevel() - 1)
      local usage = ability:GetLevelSpecialValueFor("usage_rate", level)
      if usage ~= 0 then
        abilities[#abilities+1] = {ab = ability, usage = usage, mult = ability:GetLevelSpecialValueFor("usage_mult",level)}
      end
    end
  end)

  return setmetatable({entity = entity, abilities = abilities}, self)
end

function ai:startTurn()
  if self.entity:IsNull() then return end

  local enemies = FindUnitsInRadius(DOTA_TEAM_BADGUYS, self.entity:GetOrigin(), nil, 2000, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO, 0, 0, false )

  if #enemies <= 0 then
    CombatManager:AddTurnAP(10)
    return CombatManager:EndTurn(self.entity:entindex())
  end


  self.target = nil

  -- Get closest enemy
  local min = 99999
  for _,v in pairs(enemies) do
    if v.inCombat then
      local range = self.entity:GetRangeToUnit(v)
      if range < min then
        self.target = v
        min = range
      end
    end
  end

  if not self.target then
    CombatManager:AddTurnAP(10)
    return CombatManager:EndTurn(self.entity:entindex())
  end

  local order

  --  Check for possible Casts
  for _,v in pairs(self.abilities) do
    if v.ab:IsFullyCastable() and RandomInt(1, 100) < v.usage then
      self.action = v.ab

      v.usage = v.ab:GetLevelSpecialValueFor("usage_rate", (v.ab:GetLevel() - 1))

      order = {
        OrderType = DOTA_UNIT_ORDER_CAST_TARGET,
        UnitIndex = self.entity:entindex(),
        TargetIndex = self.target:entindex(),
        AbilityIndex = v.ab:entindex()
      }
    else
      v.usage = v.usage * v.mult
    end
  end

  --  Basic Attack
  if not order then
    self.action = "attack"
    order = {
      OrderType = DOTA_UNIT_ORDER_ATTACK_TARGET,
      UnitIndex = self.entity:entindex(),
      TargetIndex = self.target:entindex(),
    }
  end

  self.entity:AddNewModifier(self.entity, nil, "modifier_phased", {})

  ExecuteOrderFromTable( order )
end

function ai:finishedMove()
  self.entity:RemoveModifierByName("modifier_phased")

  local range
  if self.action == "attack" then
    range = self.entity:GetAttackRange()
  else
    range = self.entity:GetAttackRange()
    -- range = self.action:
  end


  if self.entity:GetRangeToUnit(self.target) > range then
    CombatManager:AddTurnAP(10)
    Timers:CreateTimer(0.5, function()
      GameRules.CombatManager:EndTurn(self.entity:entindex())
    end)
  end
end

function ai:finishedAction()
  if self.action == "stun" then self.abilities.stun.cd = self.abilities.stun.maxCd end
  if self.action == "fire" then self.abilities.fire.cd = self.abilities.fire.maxCd end

  self.entity:RemoveModifierByName("modifier_phased")

  Timers:CreateTimer(0.5, function()
      GameRules.CombatManager:EndTurn(self.entity:entindex())
    end
  )
end

thisEntity.ai = ai:start(thisEntity)