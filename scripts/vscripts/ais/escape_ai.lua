require ("timers")

local ai = {}
ai.__index = ai

function ai:start(entity)
  local abilities = {}

  Timers:CreateTimer(0.5, function()
    if entity:IsNull() then return end
    
    for i = 0, entity:GetAbilityCount() do
      local ability = entity:GetAbilityByIndex(i)
      if not ability then break end

      local level = (ability:GetLevel() - 1)
      local usage = ability:GetLevelSpecialValueFor("usage_rate", level)
      if usage ~= 0 then
        abilities[#abilities+1] = {ab = ability, usage = usage, mult = ability:GetLevelSpecialValueFor("usage_mult",level)}
      end
    end
  end)

  local aux = GameRules.unitKV[entity:GetUnitName()]
  if not aux then return print("shits fucked up with unitKV") end

  local level = entity:GetLevel()

  entity.atkSpeed = type(aux.AtkSpeed) == "string" and aux.AtkSpeed:split(" ")[level] or aux.atkSpeed
  entity.movType = GameRules.movTypes[aux.MovementCapabilities]
  entity.haste = 0

  return setmetatable({entity = entity, abilities = abilities, turn = 0}, self)
end

function ai:startTurn()
  if self.entity:IsNull() then return end

  self.target = nil

  -- Start target based on the closest enemy
  if not self.target then
    local enemies = FindUnitsInRadius(DOTA_TEAM_BADGUYS, self.entity:GetOrigin(), nil, 2000, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO, 0, 0, false )

    if #enemies <= 0 then
      CombatManager:AddTurnAP(10)
      return CombatManager:EndTurn(self.entity:entindex())
    end

    -- Get closest enemy
    local min = 99999
    for _,v in pairs(enemies) do
      if v.inCombat then
        local range = self.entity:GetRangeToUnit(v)
        if range < min then
          self.target = v:GetAbsOrigin()
          min = range
        end
      end
    end
  end

  if not self.target then
    CombatManager:AddTurnAP(10)
    return CombatManager:EndTurn(self.entity:entindex())
  end

  -- Escape route
  self.action = "move"
  self.turn = self.turn + 1

  local vec = (self.target - GameRules.combatCenter):Normalized()
  local dir
  if vec:Length2D() == 0 then
    dir = (self.entity:GetAbsOrigin() - self.target):Normalized()
  else
    local rot = QAngle (0, self.entity:GetIdealSpeed() / 15 * self.turn, 0)
    dir = RotatePosition(Vector(0,0,0), rot, vec)
  end

  self.target = GameRules.combatCenter + dir * 900

  -- Move to position
  local order = {
    OrderType = DOTA_UNIT_ORDER_MOVE_TO_POSITION,
    UnitIndex = self.entity:entindex(),
    Position = self.target,
  }
  ExecuteOrderFromTable( order )

  local pos = self.entity:GetAbsOrigin()
  local num = GameRules.curPhase

  Timers:CreateTimer(3, function()
    if num ~= GameRules.curPhase or not self.action or self.entity:IsNull() then return end
    
    if (pos - self.entity:GetAbsOrigin()):Length2D() <= 10 then
      return Cast(self)
    end
    pos = self.entity:GetOrigin()

    return 0.7
  end)

  self.entity:AddNewModifier(self.entity, nil, "modifier_phased", {})

  ExecuteOrderFromTable( order )
end

function ai:finishedMove()
  self.action = nil

  Cast(self)
end

function Cast(self)
  self.entity:RemoveModifierByName("modifier_phased")
  
  --  Check for possible Casts
  for _,v in pairs(self.abilities) do
    if v.ab:IsFullyCastable() and RandomInt(1, 100) < v.usage then
      self.action = v.ab

      v.usage = v.ab:GetLevelSpecialValueFor("usage_rate", (v.ab:GetLevel() - 1))

      return ExecuteOrderFromTable({
        OrderType = DOTA_UNIT_ORDER_CAST_NO_TARGET,
        UnitIndex = self.entity:entindex(),
        AbilityIndex = v.ab:entindex()
      })
    else
      v.usage = v.usage * v.mult
    end
  end

  CombatManager:AddTurnAP(10)
  Timers:CreateTimer(0.5, function()
    if self.entity:IsNull() then return end
    GameRules.CombatManager:EndTurn(self.entity:entindex())
  end)
end

function ai:finishedAction()
  Timers:CreateTimer(0.5, function()
    if self.entity:IsNull() then return end
    GameRules.CombatManager:EndTurn(self.entity:entindex())
  end)
end

-- For dinamic changes
if thisEntity then
  thisEntity.ai = ai:start(thisEntity)
else
  return ai
end