require ("timers")

local ai = {}
ai.__index = ai

function ai:start(entity)
  local abilities = {}

  local aux = GameRules.unitKV[entity:GetUnitName()]
  if not aux then return print("shits fucked up with unitKV") end

  local level = entity:GetLevel()

  entity.atkSpeed = type(aux.AtkSpeed) == "string" and aux.AtkSpeed:split(" ")[level] or aux.atkSpeed
  entity.movType = GameRules.movTypes[aux.MovementCapabilities]
  entity.haste = 0
  
  return setmetatable({entity = entity, abilities = abilities}, self)
end

function ai:startTurn()
  if self.entity:IsNull() then return end

  self.action = nil
  local order

  order = Heal(self)

  if not order then order = Attack(self) end
  if not order then
    CombatManager:AddTurnAP(10)
    return CombatManager:EndTurn(self.entity:entindex())
  end

  self.entity:AddNewModifier(self.entity, nil, "modifier_phased", {})

  local order = 
  ExecuteOrderFromTable( order )
end

function Attack(self)
  local enemies = FindUnitsInRadius(self.entity:GetTeam(), self.entity:GetOrigin(), nil, 2000, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_ALL, 0, 0, false )
  self.target = nil

  -- Get closest enemy
  local min = 99999
  for _,v in pairs(enemies) do
    if v.inCombat then
      local range = self.entity:GetRangeToUnit(v)
      if range < min then
        self.target = v
        min = range
      end
    end
  end
  
  if not self.target then return end

  self.action = "attack"

  return {
    OrderType = DOTA_UNIT_ORDER_ATTACK_TARGET,
    UnitIndex = self.entity:entindex(),
    TargetIndex = self.target:entindex(),
  }
end

function Heal(self)
  local ability
  local found = false
  for i = 0, self.entity:GetAbilityCount() do
    ability = self.entity:GetAbilityByIndex(i)

    if ability and ability:GetAbilityTargetTeam() == DOTA_UNIT_TARGET_TEAM_FRIENDLY then
      found = true
      break
    end
  end

  if not found or not ability:IsFullyCastable() then return end

  local friends = FindUnitsInRadius(self.entity:GetTeam(), self.entity:GetOrigin(), nil, 2000, DOTA_UNIT_TARGET_TEAM_FRIENDLY, DOTA_UNIT_TARGET_ALL, 0, 0, false)
  if #friends <= 0 then return end

  -- Get closest enemy
  local max = 0
  for _,v in pairs(friends) do
    local miss = v:GetHealthDeficit()
    if miss > max then
      self.target = v
      max = miss
    end
  end
  if max == 0 then return end

  self.action = "Heal"

  return {
    OrderType = DOTA_UNIT_ORDER_CAST_TARGET,
    UnitIndex = self.entity:entindex(),
    TargetIndex = self.target:entindex(),
    AbilityIndex = ability:entindex()
  }
end

function ai:finishedMove()
  self.entity:RemoveModifierByName("modifier_phased")

  if self.entity:GetRangeToUnit(self.target) > self.entity:GetAttackRange() then
    CombatManager:AddTurnAP(10)
    
    Timers:CreateTimer(0.5, function()
        GameRules.CombatManager:EndTurn(self.entity:entindex())
      end
    )
  end
end

function ai:finishedAction()
  self.entity:RemoveModifierByName("modifier_phased")
  Timers:CreateTimer(0.5, function()
      GameRules.CombatManager:EndTurn(self.entity:entindex())
    end
  )
end

thisEntity.ai = ai:start(thisEntity)