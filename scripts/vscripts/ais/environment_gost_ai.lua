require ("timers")

local ai = {}
ai.__index = ai

function ai:start(entity)
  local abilities = {}

  local name = entity:GetUnitName()
  if name == "" then return end

  local aux = GameRules.unitKV[name]
  if not aux then return print("shits fucked up with unitKV") end

  local level = entity:GetLevel()

  entity.atkSpeed = type(aux.AtkSpeed) == "string" and aux.AtkSpeed:split(" ")[level] or aux.atkSpeed
  entity.movType = GameRules.movTypes[aux.MovementCapabilities]
  entity.haste = 0
  
  return setmetatable({entity = entity, abilities = abilities}, self)
end

function ai:startTurn()
  if self.entity:IsNull() then return end

  local enemies = FindUnitsInRadius(self.entity:GetTeam(), self.entity:GetOrigin(), nil, 2000, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_ALL, 0, 0, false )
  self.target = nil

  -- Get closest enemy
  local min = 99999
  for _,v in pairs(enemies) do
    if v.inCombat then
      local range = self.entity:GetRangeToUnit(v)
      if range < min then
        self.target = v
        min = range
      end
    end
  end

  local rand = RandomInt(1, #self.entity.spirits)
  local unit = self.entity.spirits[rand]

  if not unit then return print("no spirits found") end

  unit.state = "target_acquired"
  unit.current_target = self.target
  unit.targetPos = unit.current_target:GetAbsOrigin()
end

function ai:finishedMove()
end

function ai:finishedAction()
  CombatManager:AddTurnAP(self.entity.atkSpeed)
  Timers:CreateTimer(0.5, function()
    GameRules.CombatManager:EndTurn(self.entity:entindex())
  end)
end

thisEntity.ai = ai:start(thisEntity)