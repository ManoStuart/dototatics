require ("timers")

if not GameRules.unitKV then
  GameRules.unitKV = LoadKeyValues("scripts/npc/npc_units_custom.txt")
  GameRules.movTypes = {DOTA_UNIT_CAP_MOVE_NONE = 0, DOTA_UNIT_CAP_MOVE_GROUND = 1, DOTA_UNIT_CAP_MOVE_FLY = 2}

  function string:split(sep)
    local sep, fields = sep or ":", {}
    local pattern = string.format("([^%s]+)", sep)
    self:gsub(pattern, function(c) fields[#fields+1] = c end)
    return fields
  end
end

local ai = {}
ai.__index = ai

function ai:start(entity)
  local abilities = {}
  
  local name = entity:GetUnitName()
  if name == "" then return end

  local aux = GameRules.unitKV[name]
  if not aux then return print("shits fucked up with unitKV") end

  local level = entity:GetLevel()

  entity.atkSpeed = type(aux.AtkSpeed) == "string" and aux.AtkSpeed:split(" ")[level] or aux.atkSpeed
  entity.movType = GameRules.movTypes[aux.MovementCapabilities]
  entity.haste = 0

  return setmetatable({entity = entity, abilities = abilities}, self)
end

function ai:startTurn()
  if self.entity:IsNull() then return end

  local enemies = FindUnitsInRadius(self.entity:GetTeam(), self.entity:GetOrigin(), nil, 2000, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_ALL, 0, 0, false )

  self.target = nil

  -- Get closest enemy
  local min = 99999
  for _,v in pairs(enemies) do
    if v.inCombat then
      local range = self.entity:GetRangeToUnit(v)
      if range < min then
        self.target = v
        min = range
      end
    end
  end
  
  if not self.target then
    CombatManager:AddTurnAP(10)
    return CombatManager:EndTurn(self.entity:entindex())
  end

  self.action = "attack"

  self.entity:AddNewModifier(self.entity, nil, "modifier_phased", {})

  local order = {
    OrderType = DOTA_UNIT_ORDER_ATTACK_TARGET,
    UnitIndex = self.entity:entindex(),
    TargetIndex = self.target:entindex(),
  }
  ExecuteOrderFromTable( order )
end

function ai:finishedMove()
  self.entity:RemoveModifierByName("modifier_phased")

  -- Attacked and is running back
  if self.action ~= "attack" then
    -- action_modifiers takes care of calling EndTurn
    return

  -- Not in range to attack
  elseif self.entity:GetRangeToUnit(self.target) > self.entity:GetAttackRange() then
    CombatManager:AddTurnAP(10)
    
    Timers:CreateTimer(0.5, function()
      GameRules.CombatManager:EndTurn(self.entity:entindex())
    end)
  end
end

function ai:finishedAction()
  self.action = "move"

  local vec = (self.entity:GetOrigin() - self.target:GetOrigin()):Normalized()

  -- Move to position
  local order = {
    OrderType = DOTA_UNIT_ORDER_MOVE_TO_POSITION,
    UnitIndex = self.entity:entindex(),
    Position = self.entity:GetOrigin() + vec * self.entity:GetIdealSpeed(),
  }
  ExecuteOrderFromTable( order )

  local pos = self.entity:GetOrigin()
  local num = GameRules.curPhase

  Timers:CreateTimer(0.5, function()
    if num ~= GameRules.curPhase then return end
    
    if (pos - self.entity:GetOrigin()):Length2D() <= 10 then
      self.entity:Stop()
      GameRules.CombatManager:EndTurn(self.entity:entindex())
      return
    end
    pos = self.entity:GetOrigin()

    return 0.5
  end)
end

thisEntity.ai = ai:start(thisEntity)