require ("timers")

local ai = {}
ai.__index = ai

function ai:start(entity)
  local abilities = {}

  local aux = GameRules.unitKV[entity:GetUnitName()]
  if not aux then return print("shits fucked up with unitKV") end

  local level = entity:GetLevel()

  entity.atkSpeed = type(aux.AtkSpeed) == "string" and aux.AtkSpeed:split(" ")[level] or aux.atkSpeed
  entity.movType = GameRules.movTypes[aux.MovementCapabilities]
  entity.haste = 0
  
  return setmetatable({entity = entity, abilities = abilities}, self)
end

function ai:startTurn()
  if self.entity:IsNull() then return end

  local enemies = FindUnitsInRadius(self.entity:GetTeam(), self.entity:GetOrigin(), nil, 2000, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_ALL, 0, 0, false )
  self.target = nil

  -- Get weaker enemy
  local min = 99999
  for _,v in pairs(enemies) do
    if v.inCombat then
      local range = self.entity:GetHealth()
      if range < min then
        self.target = v
        min = range
      end
    end
  end
  
  if not self.target then
    CombatManager:AddTurnAP(10)
    return CombatManager:EndTurn(self.entity:entindex())
  end
  
  self.action = "attack"

  self.entity:AddNewModifier(self.entity, nil, "modifier_phased", {})

  local order = {
    OrderType = DOTA_UNIT_ORDER_ATTACK_TARGET,
    UnitIndex = self.entity:entindex(),
    TargetIndex = self.target:entindex(),
  }
  ExecuteOrderFromTable( order )
end

function ai:finishedMove()
  self.entity:RemoveModifierByName("modifier_phased")

  if self.entity:GetRangeToUnit(self.target) > self.entity:GetAttackRange() then
    CombatManager:AddTurnAP(10)
    
    Timers:CreateTimer(0.5, function()
      GameRules.CombatManager:EndTurn(self.entity:entindex())
    end)
  end
end

function ai:finishedAction()
  self.entity:RemoveModifierByName("modifier_phased")
  Timers:CreateTimer(0.5, function()
    GameRules.CombatManager:EndTurn(self.entity:entindex())
  end)
end

thisEntity.ai = ai:start(thisEntity)