require ("timers")

local ai = {}
ai.__index = ai

function ai:start(entity)
  entity.isBoss = 1

  local abilities = {}

  Timers:CreateTimer(0.5, function()
    if entity:IsNull() then return end
    
    abilities.swarm = {ab = entity:FindAbilityByName("death_prophet_carrion_swarm"), cd = 0, maxCd = 4}
  end)

  local aux = GameRules.unitKV[entity:GetUnitName()]
  if not aux then return print("shits fucked up with unitKV") end

  local level = entity:GetLevel()

  entity.atkSpeed = type(aux.AtkSpeed) == "string" and aux.AtkSpeed:split(" ")[level] or aux.atkSpeed
  entity.movType = GameRules.movTypes[aux.MovementCapabilities]
  entity.haste = 0

  return setmetatable({entity = entity, abilities = abilities}, self)
end

function ai:startTurn()
  if self.entity:IsNull() then return end

  -- Reduce abilities cooldown
  for _,v in pairs(self.abilities) do
    v.cd = v.cd - 1
  end

  local enemies = FindUnitsInRadius(DOTA_TEAM_BADGUYS, self.entity:GetOrigin(), nil, 2000, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO, 0, 0, false )

  if #enemies <= 0 then
    CombatManager:AddTurnAP(10)
    return CombatManager:EndTurn(self.entity:entindex())
  end

  self.entity:AddNewModifier(self.entity, nil, "modifier_phased", {})

  -- Use swarm
  if #enemies >= 2 and self.abilities.swarm.cd <= 0 then
    for _,v in pairs(enemies) do
      for _,v2 in pairs(enemies) do

        -- Find Enemies in line
        if v ~= v2 and (v:GetOrigin() - v2:GetOrigin()):Length2D() < 500 then
          if (v:GetOrigin() - self.entity:GetOrigin()):Length2D() > (v2:GetOrigin() - self.entity:GetOrigin()):Length2D() then
            local aux = v
            v = v2
            v2 = aux
          end

          local pos = v:GetOrigin() + (v:GetOrigin() - v2:GetOrigin()):Normalized() * 100
          self.target = v
          self.action = "swarm"

          -- Move to position
          local order = {
            OrderType = DOTA_UNIT_ORDER_MOVE_TO_POSITION,
            UnitIndex = self.entity:entindex(),
            Position = pos,
          }
          ExecuteOrderFromTable( order )

          local order = {
            OrderType = DOTA_UNIT_ORDER_CAST_TARGET,
            UnitIndex = self.entity:entindex(),
            TargetIndex = self.target:entindex(),
            AbilityIndex = self.abilities.swarm.ab:entindex()
          }
          return Timers:DelayedOrder(order, 1)
        end
      end
    end
  end
  
  self.target = nil

  -- Get closest enemy
  local min = 99999
  for _,v in pairs(enemies) do
    if v.inCombat then
      local range = self.entity:GetRangeToUnit(v)
      if range < min then
        self.target = v
        min = range
      end
    end
  end
  
  if not self.target then
    CombatManager:AddTurnAP(10)
    return CombatManager:EndTurn(self.entity:entindex())
  end

  --  Basic Attack
  self.action = "attack"

  local order = {
    OrderType = DOTA_UNIT_ORDER_ATTACK_TARGET,
    UnitIndex = self.entity:entindex(),
    TargetIndex = self.target:entindex(),
  }
  ExecuteOrderFromTable( order )
end

function ai:finishedMove()
  self.entity:RemoveModifierByName("modifier_phased")

  if self.entity:GetRangeToUnit(self.target) > self.entity:GetAttackRange() then
    CombatManager:AddTurnAP(10)
    Timers:CreateTimer(0.5, function()
        GameRules.CombatManager:EndTurn(self.entity:entindex())
      end
    )
  end
end

function ai:finishedAction()
  if self.action == "swarm" then self.abilities.swarm.cd = self.abilities.swarm.maxCd end

  self.entity:RemoveModifierByName("modifier_phased")

  Timers:CreateTimer(0.5, function()
      GameRules.CombatManager:EndTurn(self.entity:entindex())
    end
  )
end

thisEntity.ai = ai:start(thisEntity)